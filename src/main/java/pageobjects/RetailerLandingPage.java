package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import utils.WebElementHandler;

public class RetailerLandingPage {

    WebDriver driver;
    SoftAssert softAssert;
    String retailerName;

    private WebElement adsContainerSection = null;
    private List<WebElement> individualAdDivList = null;
    private WebElement shopOnlineButton = null;
    private WebElement saveToWalletButton = null;
    private WebElement findAStoreButton = null;
    private WebElement fallbackAd = null;
    private WebElement firstIndividualAdDiv = null;

//    ObjectActions oa;

    @FindBy(xpath="(//div[@id='container'])")
    public WebElement containerSection;

    @FindBy (xpath="(//div[@id='header'])")
    public WebElement headerSection;

    @FindBy (xpath="(//img[@id='headerLogo'])")
    public WebElement headerLogo;

    @FindBy (xpath="(//*[@id='adcontainer'])")
    public WebElement meijerAdsContainerSection;

    @FindBy (xpath="(//ads-container2[@id='adcontainer'])")
    public WebElement weisAdsContainerSection;

    @FindBy (xpath="(//ads-container2[@id='adcontainer'])")
    public WebElement giantAdsContainerSection;

    @FindBy (xpath="(//div[@id='dates'])")
    public WebElement datesDiv;

    @FindBy (xpath="(//div[@id='dates']//span[1])")
    public WebElement dealsValiditySpan;

    @FindBy (xpath="(//div[@id='dates']//span[2])")
    public WebElement retailerCardSpan;

    @FindBy (xpath="(//div[@class='individualAdDiv au-target'])")
    public List<WebElement> meijerIndividualAdDivList;

    @FindBy (xpath="(//div[@class='individualAdDiv'])")
    public List<WebElement> weisIndividualAdDivList;

    @FindBy (xpath="(//div[@class='individualAdDiv'])")
    public List<WebElement> giantIndividualAdDivList;

    @FindBy (xpath="(//div[@class='individualAdDivRightContent'])")
    public List<WebElement> individualAdDivRightContentList;

    @FindBy (xpath="(//div[@id='footer'])")
    public WebElement footerSection;

    @FindBy (xpath="//img[@alt='Shop online']")
    public WebElement meijerShopOnlineButton;

    @FindBy (xpath="//a[@class='fullwidth au-target' and @href='https://www.weismarkets.com/shop']//img[@class='fullwidth au-target' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/45/shop-online.png']")
    public WebElement weisShopOnlineButton;

    @FindBy (xpath="//a[@class='fullwidth au-target' and @href='https://giantfoodstores.com/shopping-list']//img[@class='fullwidth au-target' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/6.1/shop-online.png']")
    public WebElement giantShopOnlineButton;

    @FindBy (xpath="//img[@alt='Save to wallet']")
    public WebElement meijerSaveToWalletButton;

    @FindBy (xpath="//a[@class='fullwidth au-target']//img[@class='fullwidth au-target' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/45/save-to-wallet.png']")
    public WebElement weisSaveToWalletButton;

    @FindBy (xpath="//a[@class='fullwidth au-target']//img[@class='fullwidth au-target' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/6.1/save-to-wallet.png']")
    public WebElement giantSaveToWalletButton;

    @FindBy (xpath="//img[@alt='Find a store']")
    public WebElement meijerFindAStoreButton;

    @FindBy (xpath="//img[@id='findastorebutton' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/45/find-a-store.png']")
    public WebElement weisFindAStoreButton;

    @FindBy (xpath="//img[@id='findastorebutton' and @src='https://cdn-digital.catalina.com/circp-sqa/landing-page/6.1/find-a-store.png']")
    public WebElement giantFindAStoreButton;

    @FindBy (xpath="//div[@class='individualAdDivLeft'][1]//*[starts-with(@src,'https://cdn-digital.catalina.com/circp-sqa/landing-page')]")
    public WebElement meijerFallbackAd;

    @FindBy (xpath="//div[@class='individualAdDivLeft'][1]//img[@src='https://cdn-digital.catalina.com/circp-sqa/landing-page/45/fallback-ad.png']")
    public WebElement weisFallbackAd;

    @FindBy (xpath="//div[@class='individualAdDivLeft'][1]//img[@src='https://cdn-digital.catalina.com/circp-sqa/landing-page/6.1/fallback-ad.png']")
    public WebElement giantFallbackAd;

    @FindBy (xpath="(//*[contains(@class,'individualAdDiv')][1])")
    public WebElement meijerFirstIndividualAdDiv;

    @FindBy (xpath="//div[@class='individualAdDiv'][1]")
    public WebElement weisFirstIndividualAdDiv;

    @FindBy (xpath="//div[@class='individualAdDiv'][1]")
    public WebElement giantFirstIndividualAdDiv;

//    public RetailerLandingPage(SharedResource sharedResource)
//    {
//        this.driver = sharedResource.getWebDriver();
//        PageFactory.initElements(this.driver, this);
//        this.softAssert = sharedResource.getSoftAssertObject();
//    }

    public RetailerLandingPage(WebDriver driver, SoftAssert softAssert)
    {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
        this.softAssert = softAssert;
//        this.retailerName = retailerName;

//        if (retailerName.contains("weis")) {
//            adsContainerSection = weisAdsContainerSection;
//            individualAdDivList = weisIndividualAdDivList;
//            shopOnlineButton = weisShopOnlineButton;
//            saveToWalletButton = weisSaveToWalletButton;
//            findAStoreButton = weisFindAStoreButton;
//            fallbackAd = weisFallbackAd;
//            firstIndividualAdDiv = weisFirstIndividualAdDiv;
//        }
//        else
//            if (retailerName.contains("Meijer")) {
            adsContainerSection = meijerAdsContainerSection;
            individualAdDivList = meijerIndividualAdDivList;
            shopOnlineButton = meijerShopOnlineButton;
            saveToWalletButton = meijerSaveToWalletButton;
            findAStoreButton = meijerFindAStoreButton;
            fallbackAd = meijerFallbackAd;
            firstIndividualAdDiv = meijerFirstIndividualAdDiv;
//        }
//        else if (retailerName.contains("giant")) {
//            adsContainerSection = giantAdsContainerSection;
//            individualAdDivList = giantIndividualAdDivList;
//            shopOnlineButton = giantShopOnlineButton;
//            saveToWalletButton = giantSaveToWalletButton;
//            findAStoreButton = giantFindAStoreButton;
//            fallbackAd = giantFallbackAd;
//            firstIndividualAdDiv = giantFirstIndividualAdDiv;
//        }
//        else if (retailerName.contains("hannaford")) {
//            // To be modified for Hannaford
//            Assert.assertTrue(false, "hannaford settings not configured in code.");
//        }
//        else if (retailerName.contains("shoprite")) {
//            // To be modified for Shoprite
//            Assert.assertTrue(false, "shoprite settings not configured in code.");
//        }
    }

    public void verifyOffersCountOnLandingPageIsEven(int offersCountInDb) {
        int actualOffersCountOnLandingPage = individualAdDivList.size();
        int expectedOffersCountOnLandingPage = offersCountInDb;

        if (offersCountInDb % 2 == 1 && offersCountInDb != 0) {
            expectedOffersCountOnLandingPage = offersCountInDb - 1;
        }

        softAssert.assertEquals(expectedOffersCountOnLandingPage, actualOffersCountOnLandingPage);
//        softAssert.assertEquals(expectedOffersCountOnLandingPage, 98);
//        softAssert.assertAll();
    }

    public String getRetailerCardSpanText() {
        return retailerCardSpan.getText();
    }

    public String getDealsValiditySpanText() {
        return dealsValiditySpan.getText();
    }

    public boolean verifyFirstOfferHasFallbackOfferCreativeOnLandingPage() {
        WebElementHandler.waitForPageLoad(driver, 50);
        boolean isFallbackAdDisplayed = false;

        try {
            isFallbackAdDisplayed = fallbackAd.isDisplayed();
        }
        catch (NoSuchElementException e) {
            System.out.println(e);
        }
        System.out.println("isFallbackAdDisplayed: " + isFallbackAdDisplayed);
        // Scroll down a bit to capture the fallback offer creative properly
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", firstIndividualAdDiv);

        return isFallbackAdDisplayed;
    }

    public LinkedHashMap getOffersDetailsLinkedHashmapFromLandingPage() {
        int actualOffersCountOnLandingPage = individualAdDivList.size();
        LinkedHashMap<String,String> map=new LinkedHashMap<String,String>();
        WebElementHandler.waitForPageLoad(driver, 30);

        for (int i=1; i <= actualOffersCountOnLandingPage; i++) {

//            We are comparing below four fields/details of an offer from landing page against shopliftrUserOffer mongoDB collection:
//            "customPrice" in shopliftrOffer mongoDB collection represents "adDiscountText" on landing page, e.g.: "2 for $9",
//            "brand" in shopliftrOffer mongoDB collection represents "adVerbiage1" on landing page, e.g.: "Larabar",
//            "name" in shopliftrOffer mongoDB collection represents "adVerbiage2" on landing page, e.g.: "Fruit & Nut Bar",
//            "additional" shopliftrOffer in mongoDB collection represents "adAdQualifier" on landing page, e.g.: "Selected Varieties Only",

            String adDiscountTextXpath = " //*[@id=\"adcontainer\"]/div["+(i+1)+"]/div[2]/div/div[1]";
            //"(//div[@class='individualAdDivRightContent'])[" + i + "]//div[@class='adDiscountText']";
            String adVerbiage1Xpath = " //*[@id=\"adcontainer\"]/div["+(i+1)+"]/div[2]/div/div[2]";
            //"(//div[@class='individualAdDivRightContent'])[" + i + "]//div[@class='adVerbiage1']";
            String adVerbiage2Xpath =" //*[@id=\"adcontainer\"]/div["+(i+1)+"]/div[2]/div/div[3]";
            //"(//div[@class='individualAdDivRightContent'])[" +  i + "]//div[@class='adVerbiage2']";
            String adAdQualifierXpath =" //*[@id=\"adcontainer\"]/div["+(i+1)+"]/div[2]/div/div[4]";
            //"(//div[@class='individualAdDivRightContent'])[" + i + "]//div[@class='adAdQualifier']";

            String adDiscountText = "";
            String adVerbiage1 = "";
            String adVerbiage2 = "";
            String adAdQualifier = "";

            try {
//                Thread.sleep(5);
                driver.findElement(By.xpath(adDiscountTextXpath));
                adDiscountText = driver.findElement(By.xpath(adDiscountTextXpath)).getText();
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
                adDiscountText="";
            }
            try {
                driver.findElement(By.xpath(adVerbiage1Xpath));
                adVerbiage1 = driver.findElement(By.xpath(adVerbiage1Xpath)).getText();
            }catch (NoSuchElementException e) {

                adVerbiage1="";
            }
            try {
                driver.findElement(By.xpath(adVerbiage2Xpath));
                adVerbiage2 = driver.findElement(By.xpath(adVerbiage2Xpath)).getText();
            }catch (NoSuchElementException e) {

                adVerbiage2="";
            }
            try{
                driver.findElement(By.xpath(adAdQualifierXpath));
                adAdQualifier = driver.findElement(By.xpath(adAdQualifierXpath)).getText();

            }   catch (NoSuchElementException e) {

                adAdQualifier="";
            }
            System.out.println("Offer Details are "+i+"  "+adDiscountText +"  "+adVerbiage1+"  "+adVerbiage2+"  "+adAdQualifier);

            map.put("adDiscountText_"  + i, adDiscountText);
            map.put("adVerbiage1_"  + i, adVerbiage1);
            map.put("adVerbiage2_"  + i, adVerbiage2);
            map.put("adAdQualifier_"  + i, adAdQualifier);
        }

        return map;
    }

    public boolean verifyOrderOfElementsOnRetailerLandingPage() {
        boolean isOrderMatched = false;

        WebElementHandler.waitForPageLoad(driver, 30);

        boolean isContainerSectionDisplayed = WebElementHandler.isWebElementDisplayed(driver, containerSection);
        System.out.println("isContainerSectionDisplayed: " + isContainerSectionDisplayed);

        boolean isHeaderSectionDisplayed = WebElementHandler.isWebElementDisplayed(driver, headerSection);
        System.out.println("isHeaderSectionDisplayed: " + isHeaderSectionDisplayed);

        boolean isHeaderLogoDisplayed = WebElementHandler.isWebElementDisplayed(driver, headerLogo);
        System.out.println("isHeaderLogoDisplayed: " + isHeaderLogoDisplayed);

        boolean isAdsContainerSectionDisplayed = WebElementHandler.isWebElementDisplayed(driver, adsContainerSection);
        System.out.println("isAdsContainerSectionDisplayed: " + isAdsContainerSectionDisplayed);

        boolean isDatesDivDisplayed = WebElementHandler.isWebElementDisplayed(driver, datesDiv);
        System.out.println("isDatesDivDisplayed: " + isDatesDivDisplayed);

        boolean isDealsValiditySpanDisplayed = WebElementHandler.isWebElementDisplayed(driver, dealsValiditySpan);
        System.out.println("isDealsValiditySpanDisplayed: " + isDealsValiditySpanDisplayed);

        boolean isRetailerCardSpanDisplayed = WebElementHandler.isWebElementDisplayed(driver, retailerCardSpan);
        System.out.println("isRetailerCardSpanDisplayed: " + isRetailerCardSpanDisplayed);

        boolean isFooterSectionDisplayed = WebElementHandler.isWebElementDisplayed(driver, footerSection);
        System.out.println("isFooterSectionDisplayed: " + isFooterSectionDisplayed);

        boolean isShopOnlineButtonDisplayed = WebElementHandler.isWebElementDisplayed(driver, shopOnlineButton);
        System.out.println("isShopOnlineButtonDisplayed: " + isShopOnlineButtonDisplayed);

        boolean isSaveToWalletButtonDisplayed = WebElementHandler.isWebElementDisplayed(driver, saveToWalletButton);
        System.out.println("isSaveToWalletButtonDisplayed: " + isSaveToWalletButtonDisplayed);

        boolean isFindAStoreButtonDisplayed = WebElementHandler.isWebElementDisplayed(driver, findAStoreButton);
        System.out.println("isFindAStoreButtonDisplayed: " + isFindAStoreButtonDisplayed);

        if (isContainerSectionDisplayed && isHeaderSectionDisplayed && isHeaderLogoDisplayed &&
                isAdsContainerSectionDisplayed && isDatesDivDisplayed && isDealsValiditySpanDisplayed &&
                isRetailerCardSpanDisplayed && isFooterSectionDisplayed && isShopOnlineButtonDisplayed &&
                isSaveToWalletButtonDisplayed && isFindAStoreButtonDisplayed) {
            // Verify order of elements on landing page

//            Assert.assertTrue(
//                    driver.findElement(By.cssSelector("div#footer > a.fullwidth au-target[href=https://www.meijer.com/mperks/coupons]")
//                    ).isDisplayed()
//            );

            isOrderMatched = true;
        }


//        Assert.assertTrue(
//                driver.findElement(By.cssSelector("div#footer > a.'fullwidth au-target'[href='https://www.meijer.com/mperks/coupons']//img.'fullwidth au-target'[src='https://cdn-digital.catalina.com/circp-sqa/meijer-shopliftr/landing-page/shop-online.png']")
//                ).isDisplayed()
//        );

        return isOrderMatched;

    }

    public void clickShopOnlineButton() {
        WebElementHandler.waitForPageLoad(driver, 30);
        WebElementHandler.clickWebElement(driver, shopOnlineButton);
    }

    public void clickSaveToWalletButton() {
        WebElementHandler.waitForPageLoad(driver, 30);
        WebElementHandler.clickWebElement(driver, saveToWalletButton);
    }

    public boolean validatecsvFileFormat(String location,String filename,String countryCode,String networkId, String flowType)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime datetime = LocalDateTime.now(ZoneOffset.UTC);
        String afterFormat = datetime.format(formatter);
        System.out.println("filename given " +filename);
        System.out.println("filename created " + "idomoo-"+countryCode+networkId+flowType+afterFormat+"_1");
        Assert.assertEquals(filename ,"idomoo-"+countryCode+networkId+flowType+afterFormat.toString().replace("-","")+"_1");
        return true;
    }

    public void readFille(String fileName) throws IOException {

    }


}

