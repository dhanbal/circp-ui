Feature: Weis Unknown Flow
  #This Feature is to validate below test cases

#  Completed
  @WeisUnknown
  Scenario Outline: Verify Retailer Landing page when celtra response is success.
    Given remove older omniUser mongo collection documents "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>"
    Then Verify order of elements on retailer landing page
    Then compare offers details on retailer landing page with shopliftrOffer mongoDB collection "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Verify deals are available until upcoming minimum of shopliftrOffer endDate "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>"
    Then Verify Sale prices are valid with your retailer Card only "<countryCode>" "<networkId>"
    Then verify omniUser mongo collection "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"

    Examples:
      | countryCode | networkId |               DID                       |
      |    USA      |     45    | EF4B2004-BEA4-423D-BAB9-2AF9E93EDA3B    |

#  Completed
  @WeisUnknown
  Scenario Outline: Verify Clicking on shop now button
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>"
    Then Verify on clicking shop online button retailer website page is seen "<countryCode>" "<networkId>"

    Examples:
      | countryCode | networkId |               DID                       |
      |    USA      |     45    | EF4B2004-BEA4-423D-BAB9-2AF9E93EDA3B    |

#  Completed
  @WeisUnknown
  Scenario Outline: Validating save to wallet button on landing page.
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>"
    Then Verify on clicking save to wallet button wallet pass page is seen "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"

    Examples:
      | countryCode | networkId |               DID                       |
      |    USA      |     45    | EF4B2004-BEA4-423D-BAB9-2AF9E93EDA3B    |

#  Completed
  @WeisUnknown
  Scenario Outline: Verify fallback offer creative for Retailer
    Given Set value of imageUrl field as empty for the first valid promotion "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Given Extract value of fields from celtra call response and verify fallback offer creative info "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>"
    Then Verify fallback offer creative on retailer landing page for the modified offer "<countryCode>" "<networkId>"
    Then Reset original value of imageUrl field for the first valid promotion "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"

    Examples:
      | countryCode | networkId |               DID                       |
      |    USA      |     45    | EF4B2004-BEA4-423D-BAB9-2AF9E93EDA3B    |



#    Other Pending UI test cases:
#  Verify deals expiration date logic for landing page (CircP Shopliftr Known flow)
#  Validate when clicking on offers on landing page, it should NOT navigate to Retailer product search page.
#  Verify "find a Store" Button on Landing page
