package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.codehaus.plexus.util.StringOutputStream;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageobjects.CSCPLoginPage;

import javax.swing.*;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CSCPLogin {
    String url;
    public static WebDriver driver;
    Properties prop;
    CSCPLoginPage cscpLoginPage;
    WebDriverWait wait;

    public CSCPLogin() {

        try {
            System.setProperty("webdriver.chrome.driver", "C:\\Users\\csauto4\\Desktop\\Catalina-WS\\circp-ui\\Drivers\\chromedriver.exe");
            driver = new ChromeDriver();
            //Added Explicit wait
            wait = new WebDriverWait(driver,10);
            prop = new Properties();
            prop.load(new FileInputStream("C:\\Users\\csauto4\\Desktop\\Catalina-WS\\circp-ui\\src\\test\\resources\\application.properties"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("^Navigate to cscp url$")
    public void navigateToCscpUrl() throws  Throwable{

        try {
            // visit the CSCP url
            driver.navigate().to(prop.getProperty("cscpUrl"));
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            cscpLoginPage= new CSCPLoginPage(driver);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Then("^enter user name and password$")
    public void enter_user_name_and_password() throws Throwable {
    //Enter username and password durin login
        cscpLoginPage.getUserName().sendKeys(prop.getProperty("user"));
        cscpLoginPage.getPassword().sendKeys(prop.getProperty("pass"));

    }

    @Then("^login$")
    public boolean login () throws Throwable {

        wait.until(ExpectedConditions.elementToBeClickable(cscpLoginPage.getLoginButton()));
        cscpLoginPage.getLoginButton().click();
        //Thread.sleep(5*1000);
        return  true;
    }

    @Given("^Login to CSCP$")
    public void login_to_CSCP() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            driver.navigate().to(prop.getProperty("cscpUrl"));
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            cscpLoginPage= new CSCPLoginPage(driver);
            cscpLoginPage.getUserName().sendKeys(prop.getProperty("user"));
            cscpLoginPage.getPassword().sendKeys(prop.getProperty("pass"));
            cscpLoginPage.getLoginButton().click();
        }
        catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }
    private void logout() {
        Actions action=new Actions(driver);

    }

}
