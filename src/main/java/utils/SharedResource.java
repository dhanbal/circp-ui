package utils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.testng.asserts.SoftAssert;
//import entities.TestCaseRun;
//import TestRunner;

public class SharedResource {

    WebDriver driver;
    Scenario scenario;
    SoftAssert softAssert;

    //    TestRunner testRunner;
//    TargetProcess targetProcess;

//    public SharedResource() {
//        initializeWebDriver();
//        initializeSoftAssertObject();
//    }

    public WebDriver initializeWebDriver(){
        if(driver==null){
            System.out.println("In initializeWebDriver()");
            //System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }
        return driver;
    }

    public SoftAssert initializeSoftAssertObject() {
        if(softAssert == null){
            softAssert = new SoftAssert();
        }
        return softAssert;
    }

    public String getRetailerNameFromScenarioTagNames() {
        System.out.println("Scenario tags: " + scenario.getSourceTagNames());
        String retailerName = "";
        for (String tag: scenario.getSourceTagNames()) {
            tag = tag.toLowerCase();
            if (tag.contains("weis")) {
                retailerName = "weis";
            }
            else if (tag.contains("meijer")) {
                retailerName = "meijer";
            }
            else if (tag.contains("hannaford")) {
                retailerName = "hannaford";
            }
            else if (tag.contains("shoprite")) {
                retailerName = "shoprite";
            }
            else if (tag.contains("giant")) {
                retailerName = "giant";
            }

            if (!retailerName.isEmpty()) {
                break;
            }
        }
        System.out.println("RetailerNameFromScenarioTagNames: " +retailerName);
        return retailerName;
    }


//    public void initializeWebDriver() {
//        if(driver==null){
//            System.out.println("In initializeWebDriver()");
//            System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
//            driver = new ChromeDriver();
//            driver.manage().window().maximize();
//        }
//    }
//
//    public WebDriver getWebDriver() {
//        return driver;
//    }
//
//    public void initializeSoftAssertObject() {
//        if(softAssert == null){
//            softAssert = new SoftAssert();
//        }
//    }
//
//    public SoftAssert getSoftAssertObject() {
//        return softAssert;
//    }

    @Before
    public void beforeHook(Scenario scenario) throws IOException {
        this.scenario = scenario;
        System.out.println("Scenario Name--"+scenario.getName());
    }

    //@After
    public void afterHook(Scenario scenario) throws Throwable {
        System.out.println("Completed the Scenario--"+scenario.getName());
//        addScreenshot(driver);
        Reporter.addScenarioLog("Finished Scenario : " + scenario.getName());
        System.out.println("Scenario status :- "+ scenario.getStatus());
//        String name = scenario.getName();
//        if(name.contains("TestCaseIds"))
//        {
//            String[] list  = name.split("TestCaseIds");
//            String[] list1  = list[1].split(",");
//            int[] testcaseId = Arrays.asList(list1).stream().map(String::trim).mapToInt(Integer::parseInt).toArray();
//            TargetProcess tp= new TargetProcess();
//            try{
//
//                String status = this.scenario.getStatus();
//                TestCaseRun testCaseRun = new TestCaseRun(status,"Automation Update");
//                for(int i=0; i<testcaseId.length; i++)
//                {
//                    if(testRunner.buildID!=-1)
//                    {
//                        tp.updateStatus(testRunner.buildID, testcaseId[i], testCaseRun);
//                    }
//                }
//            }catch(Exception e){
//                e.printStackTrace();
//                System.out.println("Exception : In Method: UpdateTCResult() "+e.getMessage());
//            }
//        }
//        else
//        {
//            System.out.println("No id's Present");
//        }
        //softAssert.assertAll();
        //driver.close();
    }

    public Scenario getScenario() {
        return scenario;
    }

//    public void addScreenshot(WebDriver driver) throws IOException, InterruptedException {
////        scenario = getScenario();
//        long currentTimeMillis = TimeUtils.getCurrentTimeMillis();
//        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
//        File sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//
//        File destinationFolder = new File(System.getProperty("user.dir")
//                + "/target/screenshots");
//
//        if (!destinationFolder.exists()) {
//            System.out.println("Folder created: " + destinationFolder);
//            destinationFolder.mkdir();
//        }
//
//        String imagePath = System.getProperty("user.dir") + "/target/screenshots/" + currentTimeMillis + ".png";
//
//        File destinationPath = new File(imagePath);
//
//        Files.copy(sourcePath, destinationPath);
//
//        //This attach the specified screenshot to the test
//        Reporter.addScreenCaptureFromPath(imagePath);
////        Reporter.addStepLog("<img src=\"" +imagePath+ "\">");
//    }


    public void addScreenshot(WebDriver driver) throws IOException, InterruptedException {
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
        String dest = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
        //This attach the specified screenshot to the test
        Reporter.addScreenCaptureFromPath("data:image/jpg;base64, " + dest);
    }

}
