package utils;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import java.io.IOException;
import java.util.LinkedHashMap;

public class HttpResponse {
    ReadPropertyFile readPropertyFile;

    public int httpResponseCodeViaGet(String url) {
        return RestAssured.get(url).statusCode();
    }

    public int httpResponseCodeViaPost(String url) {
        return RestAssured.post(url).statusCode();
    }

    public Response httpResponseViaGet(String url) {
        return RestAssured.get(url);
    }

    public Response httpResponseViaPost(String url) throws IOException {
        return RestAssured.post(url);
    }

    public String getLandingPageUrlFromCeltraResponse(String celtraLink) throws Exception {
        Response response = RestAssured.get(celtraLink);
        int statusCode = response.getStatusCode();
        String responseBody = response.asString();
        System.out.println("statusCode: " + statusCode);
        System.out.println("Response Body: " + responseBody);
        JSONObject jsonResponse = new JSONObject(responseBody);
        String landingPageUrl = null;
        try {
            landingPageUrl = jsonResponse.getString("click_thru_url_1");
            System.out.println("click_thru_url_1: " + landingPageUrl);
        }
        catch (JSONException | NullPointerException e) {
            System.out.println(e.getMessage());
        }

        Assert.assertEquals( "Correct status code was returned", 200, statusCode);
        Assert.assertNotNull("null landingPageUrl was returned", landingPageUrl);
        return landingPageUrl;
    }

    public LinkedHashMap<String, String> extractValueOfFieldsFromCeltraCallResponseAndVerifyFallbackOfferCreativeInfo(String celtraLink, String countryCode, String networkId) throws Exception {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        Response response = RestAssured.get(celtraLink);
        int statusCode = response.getStatusCode();
        String responseBody = response.asString();
        System.out.println("statusCode: " + statusCode);
        System.out.println("Response Body: " + responseBody);
        JSONObject jsonResponse = new JSONObject(responseBody);
        String landingPageUrl = null;
        try {
            landingPageUrl = jsonResponse.getString("click_thru_url_1");
            map.put("landingPageUrl", landingPageUrl);
            System.out.println("click_thru_url_1: " + landingPageUrl);
        }
        catch (JSONException | NullPointerException e) {
            System.out.println(e.getMessage());
        }

        readPropertyFile = new ReadPropertyFile();
        String expectedFallbackOfferCreativeInfoFromCeltraCallResponse = readPropertyFile.getFallbackOfferCreativeInfoFromCeltraCallResponse(countryCode, networkId);
        Assert.assertEquals(expectedFallbackOfferCreativeInfoFromCeltraCallResponse, jsonResponse.getString("image_url_1"));
        Assert.assertEquals(expectedFallbackOfferCreativeInfoFromCeltraCallResponse, jsonResponse.getString("creative_1"));

        Assert.assertEquals( "Correct status code was returned", 200, statusCode);
        Assert.assertNotNull("null landingPageUrl was returned", landingPageUrl);
        return map;
    }

    public String getCidForDidUsingCardlinkPostCall(String countryCode, String networkId, String DID) throws Exception {
        readPropertyFile = new ReadPropertyFile();
        String xApiKey = readPropertyFile.getXApiKeyForCardlink();
        String sqaCardlinkDidToCidMappingUrl = readPropertyFile.getSQACardlinkDidToCidMappingUrl();
        LinkedHashMap<String, String> networkAndSubnetwork = readPropertyFile.getNetworkAndSubnetwork(networkId);
        String extractedNetworkFromPropertyFile = networkAndSubnetwork.get("network");
        String extractedSubnetworkFromPropertyFile = networkAndSubnetwork.get("subnetwork");
        System.out.println("@@@@@@@@@@@@@@@@@@@@@" + extractedNetworkFromPropertyFile);
        System.out.println("@@@@@@@@@@@@@@@@@@@@@" + extractedSubnetworkFromPropertyFile);
        String cardlinkNetworkCode = readPropertyFile.getCardlinkNetworkCode(extractedNetworkFromPropertyFile);


        System.out.println("DID: " + DID);
        System.out.println("x-api-key: " +xApiKey);
        System.out.println("sqaCardlinkDidToCidMappingUrl: " + sqaCardlinkDidToCidMappingUrl);
        System.out.println("cardlinkNetworkCode: " + cardlinkNetworkCode);

        RestAssured.baseURI = sqaCardlinkDidToCidMappingUrl;
        RequestSpecification request = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("identificator", DID);

        request.header("Content-Type", "application/json");
        request.header("Accept", "application/json");
        request.header("x-api-key", xApiKey);

//         Add the Json to the body of the request
        request.body(requestParams.toString());

        Response response = request.post();

        int statusCode = response.getStatusCode();
        System.out.println("statusCode: " + statusCode);
        Assert.assertEquals("DID-CID mapping status: ", 200, statusCode);

        String properJSONFormat = "{ \"data\" : " + response.body().asString() + "}";
        JSONObject jsonResponse = new JSONObject(properJSONFormat);
        System.out.println("jsonResponse: "  +jsonResponse.toString());



        JSONArray arr = jsonResponse.getJSONArray("data");
        String CID = "";
        for (int i = 0; i < arr.length(); i++)
        {
            String country = arr.getJSONObject(i).getString("country");
            String network = arr.getJSONObject(i).getString("network");
            System.out.println("----------network----------" +network);
            String subnetwork = "";
            try {
                subnetwork = arr.getJSONObject(i).getString("subnetwork");
            }
            catch (JSONException e) {
                System.out.println(e);
            }

            String identificator = arr.getJSONObject(i).getString("identificator");
            if (network.equals(cardlinkNetworkCode) && ((subnetwork.equals("") && extractedSubnetworkFromPropertyFile == null) || subnetwork.equals(extractedSubnetworkFromPropertyFile)) && country.equals(countryCode)) {
                CID = country + "-" + network + "-" + identificator;
                System.out.println("---------------------------CID: ---------------- " + CID);
                break;
            }
             else if (network.equals(cardlinkNetworkCode) && ((subnetwork.equals("2") && extractedSubnetworkFromPropertyFile.equals("2")) || subnetwork.equals(extractedSubnetworkFromPropertyFile)) && country.equals(countryCode)) {
                CID = country + "-" + network + "-" + identificator;
                System.out.println("---------------------------CID: ---------------- " + CID);
                break;
            }
            else if (network.equals(cardlinkNetworkCode) && ((subnetwork.equals("1") && extractedSubnetworkFromPropertyFile.equals("1")) || subnetwork.equals(extractedSubnetworkFromPropertyFile)) && country.equals(countryCode)) {
                CID = country + "-" + network + "-" + identificator;
                System.out.println("---------------------------CID: ---------------- " + CID);
                break;
            }

        }
        System.out.println("CID obtained from CID-DID mapping Using Cardlink Post Call: " + CID);
        Assert.assertNotNull("null CID was returned", CID);
        return CID;
    }

}
