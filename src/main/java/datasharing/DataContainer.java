package datasharing;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DataContainer {
    String CID;
    String DID;
    String celtraLink;
    String landingPageUrl;
    String documentID;
    String promotion;
    String imageUrl;
    long minimumOfShopliftrOfferEndDate;

    public DataContainer() {

    }

    public String getCID() {
        System.out.println("CID obtained from DataContainer: " + CID);
        return CID;
    }

    public void setCID(String CID) {
        System.out.println("CID set into DataContainer: " + CID);
        this.CID = CID;
    }

    public String getDID() {
        System.out.println("DID obtained from DataContainer: " + DID);
        return DID;
    }

    public void setDID(String DID) {
        System.out.println("DID set into DataContainer: " + DID);
        this.DID = DID;
    }

    public String getCeltraLink() {
        System.out.println("Celtra Link obtained from DataContainer: " + celtraLink);
        return celtraLink;
    }

    public void setCeltraLink(String celtraLink) {
        System.out.println("Celtra Link set into DataContainer: " + celtraLink);
        this.celtraLink = celtraLink;
    }

    public String getLandingPageUrl() {
        System.out.println("Landing Page Url obtained from DataContainer: " + landingPageUrl);
        return landingPageUrl;
    }

    public void setLandingPageUrl(String landingPageUrl) {
        System.out.println("Landing Page Url set into DataContainer: " + landingPageUrl);
        this.landingPageUrl = landingPageUrl;
    }

    public String getDocumentID() {
        System.out.println("Document ID obtained from DataContainer: " + documentID);
        return documentID;
    }

    public void setDocumentID(String documentID) {
        System.out.println("Document ID set into DataContainer: " + documentID);
        this.documentID = documentID;
    }

    public String getPromotion() {
        System.out.println("Promotion obtained from DataContainer: " + promotion);
        return promotion;
    }

    public void setPromotion(String promotion) {
        System.out.println("Promotion set into DataContainer: " + promotion);
        this.promotion = promotion;
    }

    public String getImageUrl() {
        System.out.println("imageUrl obtained from DataContainer: " + imageUrl);
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        System.out.println("imageUrl set into DataContainer: " + imageUrl);
        this.imageUrl = imageUrl;
    }

    public long getMinimumOfShopliftrOfferEndDate() {
        System.out.println("minimumOfShopliftrOfferEndDate obtained from DataContainer: " + minimumOfShopliftrOfferEndDate);
        return minimumOfShopliftrOfferEndDate;
    }

    public void setMinimumOfShopliftrOfferEndDate(long minimumOfShopliftrOfferEndDate) {
        System.out.println("minimumOfShopliftrOfferEndDate set into DataContainer: " + minimumOfShopliftrOfferEndDate);
        this.minimumOfShopliftrOfferEndDate = minimumOfShopliftrOfferEndDate;
    }

}
