package utils;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
public class AppTest {

    public static void appTest() throws InterruptedException {
        System.out.println("In initializeWebDriver()");
//        System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
    WebDriver driver= new ChromeDriver();

//Open the page containing video component.
driver.get("https://c.idomoo.com/4637/0001/4354778097224388c6231c1aec1d259594aeb22060d9da0316d5f18f3e564613.mp4");
//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//
//    //If video is placed in embed tag or iframe, navigate to the source.
////However, we can even switch to frame. [Else, skip this step]
//    WebElement elm = driver.findElement(By.xpath("//video[@autoplay='']"));
//
//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//
//    JavascriptExecutor jse = (JavascriptExecutor) driver;
////Click on play button
//jse.executeScript("jwplayer().play();");
//Thread.sleep(2000);
////Pause
//jse.executeScript("jwplayer().pause()");
//Thread.sleep(2000);
////Play
//jse.executeScript("jwplayer().play();");
//Thread.sleep(2000);
//// Set Volume
//Thread.sleep(2000);
//jse.executeScript("jwplayer().setVolume(50);");
//Thread.sleep(2000);
////Mute Player
//jse.executeScript("jwplayer().setMute(true);");
//Thread.sleep(2000);
////UnMute Player
//jse.executeScript("jwplayer().setMute(false);");
//Thread.sleep(2000);
////Stop the player
//jse.executeScript("jwplayer().stop()");
//Thread.sleep(2000);

driver.quit();

    }
}

