package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
//import sun.misc.InnocuousThread;
import utils.CreateBuildinTP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Hooks {



        @After
        public void afterScenario(Scenario scenario) throws IOException {
            CreateBuildinTP tp=new CreateBuildinTP();
           String [] testCaseIds=null;
           List<Integer> id=new ArrayList<Integer>();
            String scenarioName=scenario.getName();

            if(scenarioName.contains("TestcaseIds")) {
                String[] tc_ids = scenarioName.replace(" ", "").split("TestcaseIds");

                if (tc_ids.length >= 1) {
                    System.out.println("The splited value is " + tc_ids[0] + tc_ids.toString());
                    if (tc_ids[1].contains(",")) {
                        testCaseIds = tc_ids[1].split(",");
                        for (int i = 0; i < testCaseIds.length; i++) {
                            //id.add(Integer.parseInt(testCaseIds[i]));

                            if (scenario.isFailed()) {
                                tp.updateStatus(Integer.parseInt(testCaseIds[i]), "failed");

                            } else {
                                System.out.println(Integer.parseInt(testCaseIds[i]));
                                tp.updateStatus(Integer.parseInt(testCaseIds[i]), "passed");
                            }
                        }
                    } else {

                        if (scenario.isFailed()) {
//                            List<Integer> tcid=new ArrayList<Integer>();
//                            tcid.add(Integer.parseInt(testCaseIds[i]));
                            tp.updateStatus(Integer.parseInt(tc_ids[1]), "failed");

                        } else {
                            tp.updateStatus(Integer.parseInt(tc_ids[1]), "passed");
                        }
                    }
                    System.out.println("Scenario is ==>" + scenario.getName() + " " + scenario.getStatus() + " " + scenario.getId());
//                    if (scenario.isFailed()) {
//
//                        tp.updateStatus(id, "failed");
//
//                    } else {
//                        tp.updateStatus(id, "passed");
//                    }
                }
            }
            else
            {
                System.out.println("No test cases mentioned "+scenario.getName());
            }

        }
        @Before(order=0)
        public void beforeScenarioStart(Scenario scenario){
            System.out.println("-----------------Start of Scenario-----------------");
            System.out.println("Scenario is ==>"+scenario.getName()+" "+scenario.getStatus()+" "+scenario.getId());
        }
}
