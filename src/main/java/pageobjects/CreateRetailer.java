package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.xml.xpath.XPath;
import java.util.concurrent.TimeUnit;

public class CreateRetailer {

    //private  WebElement createRetailerIcon=null;
    WebElement select_retailer=null;
    WebElement goLiveDay=null;
    WebElement goliveDay;
    WebDriver driver;
    WebElement slot2High;
    WebElement slot2Low;
    WebElement slot3;
    WebElement slot3High;
    WebElement slot3Low;
    WebElement slot4;
    WebElement slot4High;
    WebElement slot4Low;
    WebElement slot5;
    WebElement slot5High;
    WebElement slot5Low;
    public CreateRetailer(WebDriver driver) {
        this.driver = driver;
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(this.driver, this);

    }



    @FindBy(xpath = "//*[@id=\"services\"]/div[2]/a/i")
    WebElement createRetailerIcon;

    @FindBy(xpath = "//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/ul/li/a")
    WebElement settingIconInCreateRetailer;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[1]/networks/span/span[1]/span/span[2]")
    WebElement createRetailerDropdown;

    @FindBy(xpath = "//*[text()='Fuel Stations - 2000']")
    WebElement fuelStation;


    public WebElement getSponsoredHigh() {
        return sponsoredHigh;
    }

    @FindBy(xpath = "//*[text()='Sponsored High']")
    WebElement sponsoredHigh;

    @FindBy(xpath = "//*[text()='Sponsored Low']")
    WebElement sponsoredLow;

    public WebElement getSponsoredLow() {
        return sponsoredLow;
    }

    //@FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/div/div/div[1]/div[1]")
    @FindBy(xpath = "//*[text()='Slot 1']")
    WebElement slot1;
    @FindBy(xpath = "//*[text()='Slot 2']")
    WebElement slot2;

//    public void setSlot(String  slot){
//        if(Integer.parseInt(slot)==2){
//            //@FindBy(xpath = "//*[text()='Slot 2']")
//
//        }
//    }

// 1 is high and 2 is low
    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/div/div/div[1]/div[3]/button[1]")
    WebElement highPriority1;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/div/div/div[1]/div[3]/button[2]")

    WebElement lowPriority1;
    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/div/div/div[2]/div[3]/button[1]")
    WebElement highPriority2;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/div/div/div[2]/div[3]/button[2]")

    WebElement lowPriority2;


    @FindBy(xpath = "//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[1]")
    WebElement retailerPageCancel;

    @FindBy(xpath ="//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[2]/input")
    WebElement noOfAdSlots;

    public WebElement getNoOfAdSlots() {
        return noOfAdSlots;
    }
//Wednesday-4 so like wise

//    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[4]")


       public void setGoliveDay(String day){
        if(day.equalsIgnoreCase("Monday"))
            goliveDay =driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[2]"));
           if(day.equalsIgnoreCase("Tuesday"))
               this.goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[3]"));
               if(day.equalsIgnoreCase("Wednesday"))
                   this.goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[4]"));
                   if(day.equalsIgnoreCase("Thursday")) {
                       System.out.println("It will be clicke on thursday");
                   goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[5]"));
                   }

                       if(day.equalsIgnoreCase("Friday"))
                           this.goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[6]"));
                           if(day.equalsIgnoreCase("Saturday"))
                               this.goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[7]"));
                               if(day.equalsIgnoreCase("Sunday"))
                                   this.goliveDay=driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/div/div[2]/div/div/div[1]"));
    }




    public WebElement getRetailerPageCancel() {
        return retailerPageCancel;
    }

    public WebElement getGoliveDay() {
        return goliveDay;
    }

    public WebElement getFuelStation() {
        return fuelStation;
    }

    public WebElement getCreateRetailerDropdown() {
        return createRetailerDropdown;
    }

    public WebElement getCreateRetailerIcon() {
        return createRetailerIcon;
    }

    public WebElement getSelect_retailer() {
        return select_retailer;
    }

    public WebElement getGoLiveDay() {
        return goLiveDay;
    }

    public WebElement getSettingIconInCreateRetailer() {
        return settingIconInCreateRetailer;
    }

    public WebElement getSlot1() {
        return slot1;
    }

    public WebElement getSlot2() {
        return slot2;
    }

    public WebElement getSlot3() {
        return slot3;
    }

    public WebElement getSlot4() {
        return slot4;
    }

    public WebElement getSlot5() {
        return slot5;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement getHighPriority1() {
        return highPriority1;
    }

    public WebElement getLowPriority1() {
        return lowPriority1;
    }

    public WebElement getHighPriority2() {
        return highPriority2;
    }

    public WebElement getLowPriority2() {
        return lowPriority2;
    }
//    public WebElement getHighPriority() {
//        return highPriority;
//    }
//
//    public WebElement getLowPriority() {
//        return lowPriority;
//    }
}
