////close jpanel , add comments\
////can pause/replay/play at given interval/screeshot
////2 file excel comparsion
//package steps;
//
//import java.awt.BorderLayout;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.UIManager;
//import com.jcraft.jsch.JSchException;
//import com.jcraft.jsch.SftpException;
//
//import uk.co.caprica.vlcj.player.component.EmbeddedMediaPlayerComponent;
//import utils.SFTPClient;
//
//public class VideoValidation extends JFrame {
//    private static final long serialVersionUID = 1L;
//    private static final String TITLE = "My First Media Player";
//    private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
////   static String purlFile = "C:\\Users\\csauto4\\Documents\\Pavitra Automation\\PURL_USA24TVKN20220102_1.csv";   
//
//    // Method to parse CSV file
//    public static List<String> csvParser(File filename) throws IOException
//    {
//        BufferedReader reader = new BufferedReader(new FileReader(filename));
//        List<String> lines = new ArrayList<>();
//        String line = null;
//        while ((line = reader.readLine()) != null) {
//            lines.add(line);
//        }
//        return lines;
//    }
//
//    //Method to read URL from CSV
//    public static List<String> readURLfromCSV(List<String> recordRowwise) {
//        List<String> urlList = new ArrayList<String>();
//        for (int i=1;i<10;i++) {
//            String[] fileRow = recordRowwise.get(i).split(",");
//            urlList.add(fileRow[28]);
//        }
//        return urlList;
//
//    }
//
//    public VideoValidation(String title) {
//        super(title);
//        mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
//    }
//
//    public void initialize() {
//        this.setBounds(100, 100, 600, 400);
//        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
//        //add comment
//
//        JPanel contentPane = new JPanel();
//        contentPane.setLayout(new BorderLayout());
//        contentPane.add(mediaPlayerComponent, BorderLayout.CENTER);
//
////      JPanel controlsPane = new JPanel();
////      contentPane.add(controlsPane, BorderLayout.SOUTH);
//
//        this.setContentPane(contentPane);
//        this.setVisible(true);
//    }
//
//    public void close()
//    {
//
//        this.dispose();
//    }
//    private void exit(int value) {
//        mediaPlayerComponent.mediaPlayer().controls().stop();
//        mediaPlayerComponent.release();
//        System.exit(value);
//
//    }
//    public void loadVideo(String path) throws InterruptedException {
//
//        mediaPlayerComponent.mediaPlayer().media().play(path);
//        Thread.sleep(3000);
//
//        boolean status = mediaPlayerComponent.mediaPlayer().status().isPlaying();
//        long duration = mediaPlayerComponent.mediaPlayer().status().time();
//        long length = mediaPlayerComponent.mediaPlayer().status().length();
////      mediaPlayerComponent.mediaPlayer().snapshots();
//
////      long interval = length / 4;
////      for(long i = 1;i <= length;i+= interval){
////    	  if(duration<interval) {
////    		  mediaPlayerComponent.mediaPlayer().controls().pause();
////    		  mediaPlayerComponent.mediaPlayer().snapshots().save(new File("C:\\Users\\csauto4\\Pictures\\Screenshots\\Snaps.png"));
////    		  mediaPlayerComponent.mediaPlayer().controls().play();
////    		  Thread.sleep(2000);
////    		  mediaPlayerComponent.mediaPlayer().controls().pause();
////    		  mediaPlayerComponent.mediaPlayer().snapshots().save(new File("C:\\Users\\csauto4\\Pictures\\Screenshots\\Snaps1.png"));
//
//
////    	  }
////      }
//        System.out.println("status: " +status);
//        System.out.println("time: " +duration);
//        System.out.println("length: " +length);
//        System.out.println("___________________________________________________________");
//    }
//    public static void main( String[] args ) throws InterruptedException, JSchException, SftpException, IOException{
//
//        //To download CSV File from SFTP Server and place it in a folder
//        SFTPClient client = new SFTPClient();
////      client.download();
//
//        //Read  Property file to pick file
//        Properties props = client.ReadPropertyFile();
//        String CSVFile=props.getProperty("localDir");
//
//        // Start video play
//        VideoValidation application = new VideoValidation(TITLE);
//
//        //Initialize Jpanel to play video
//        application.initialize();
////      application.setVisible(true);   
//
//        // Parse csv file to read Video URLs
//        List<String> recordRowwise = csvParser(new File(CSVFile));
//        List<String> urls = readURLfromCSV(recordRowwise);
//
//
//        for(String url :urls) {
//
////      Looping over each URL to play 
//            application.loadVideo(url);
//
//        }
//        Thread.sleep(5000);
//
//        //Close Jpanel
//        application.close();
//        //Release media player
//        application.exit(1);
//
//
//    }
//
//
//}