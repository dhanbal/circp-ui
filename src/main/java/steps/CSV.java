package steps;

import java.io.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.testng.Assert;
import utils.MongoDBUtils;
import utils.ReadPropertyFile;
import utils.SFTPClient;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

public class CSV {
	static ReadPropertyFile readPropertyFile;
   
    String[] HEADERS = {"unique_id", "qr_code_url", "brand_1", "name_1", "additional_1", "image_url_1", "custom_price_1", "brand_2", "name_2", "additional_2", "image_url_2", "custom_price_2", "brand_3", "name_3", "additional_3", "image_url_3", "custom_price_3", "brand_4", "name_4", "additional_4", "image_url_4", "custom_price_4", "brand_5", "name_5", "additional_5", "image_url_5", "custom_price_5"};
    String[] IDOMOOHEADERS = {"unique_id", "qr_code_url", "brand_1", "name_1", "additional_1", "image_url_1", "custom_price_1", "brand_2", "name_2", "additional_2", "image_url_2", "custom_price_2", "brand_3", "name_3", "additional_3", "image_url_3", "custom_price_3", "brand_4", "name_4", "additional_4", "image_url_4", "custom_price_4", "brand_5", "name_5", "additional_5", "image_url_5", "custom_price_5"};
//    String[] mandatoryOfferFields = {"name_1", "name_2", "name_3", "name_4", "name_5"};
    LinkedHashMap<Integer, String> errorLinkedHashMap = new LinkedHashMap<>();
    LinkedHashMap<String,Integer> uniqueIDLinkedHashMap = new LinkedHashMap<>();
    static int rowCounter = 2;  
    
    static String networkId="24";  
    static String flowType = "TVKN";  
static Properties props;
//  static String purlFile = "C:\\Users\\csauto4\\Documents\\Pavitra Automation\\idomoo-USA24TVKN20220106_1.csv";
//  static String purlFileName = "idomoo-USA24TVKN20220106_1.csv";
  String errorFile = "C:C:\\Users\\csauto4\\Documents\\Pavitra Automation\\ERROR_idomoo-USA24TVKN20220106_1.csv";
  boolean isRandomCsvValidationRequired = true;
  double stepPercentageToValidate = 10;
  double percentageOfRowsToValidatePerStep = 0.02;
    
    public static void main(String[] args) throws Throwable {
    	 readPropertyFile = new ReadPropertyFile();
    	  props = readPropertyFile.ReadCirpTVPropertyFile();
    	 String purlFile = props.getProperty("purlFile");
        
        CSV csvValidation = new CSV();        
//        SFTPClient cl= new SFTPClient();
        
        DownloadCSVFromADL csv = new DownloadCSVFromADL();
        String purlFileName = props.getProperty("purlFileName");
        csv.getCSVFromADL(purlFileName);
       csvValidation.isOrderOfHeadersCorrectInCsvFile(purlFile);
//        // Do entire csv file validation only if Order Of Headers is Correct In Csv File
    	if (csvValidation.isRandomCsvValidationRequired()) {
            csvValidation.givenCSVFile_randomValidateContents();
        }
        else  {
            csvValidation.givenCSVFile_whenRead_thenContentsAsExpected();
        }
    	 List<String> recordRowwise = csvParser(new File(purlFile));
    	 
        validate_excel_field_values_against_shopLiftrUserOffer_DB(recordRowwise, networkId,flowType);
        validateUniqueId000(recordRowwise);
	
    }
    
    public static void validateUniqueId000(List<String> recordRowwise) {  
    	boolean isUniqueIdPresent = false;     	  
    	int forCount = 0;
    	if(recordRowwise.size()<10)
    	{
    		forCount = recordRowwise.size();
    	}
    	else {
    		forCount=10;
    	}
        for(int i=1;i<forCount;i++)
        {         
        String DID = recordRowwise.get(i).split(",")[0];
        System.out.println("___________________________________ "+DID);
        if(DID.equals("00000000-0000-0000-0000-000000000000"))
        {
        	isUniqueIdPresent = true;
        	System.out.println("___________________________________ " + isUniqueIdPresent);
//        	 Assert.assertEquals(isUniqueIdPresent , true);  
        	break;
       
        }    
             
        }
      
}
    
    public static List<String> csvParser(File filename) throws IOException
    {
    	BufferedReader reader = new BufferedReader(new FileReader(filename));
    	List<String> lines = new ArrayList<>();
    	String line = null;
    	while ((line = reader.readLine()) != null) {
    	    lines.add(line);
    	}

    	return lines;
    }

    public static void validate_excel_field_values_against_shopLiftrUserOffer_DB(List<String> recordRowwise, String networkId,
		String flowType) throws Throwable {
 
		MongoDBUtils mongoDbUtils = new MongoDBUtils();
		String result = mongoDbUtils.validateidomoOffers(recordRowwise, networkId, flowType);		
		Assert.assertEquals(result, "MATCHED");
	}
    
    

    public boolean isOrderOfHeadersCorrectInCsvFile(String filename) {
        boolean isOrderOfHeadersCorrectInCsvFile = true;
        String[] actualHeaders = {};
        Reader in = null;
        try {
            in = new FileReader(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.DEFAULT.parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (CSVRecord record : records) {
            actualHeaders = new String[record.size()];
            for (int i = 0; i < actualHeaders.length; i++){
                actualHeaders[i] = record.get(i);
                // if expected column versus actual column is NOT equal, then set boolean flag as false
                if (!IDOMOOHEADERS[i].equals(actualHeaders[i])) {
                    isOrderOfHeadersCorrectInCsvFile = false;
                }
            }
            // Exit the loop after first iteration, since we want to validate just headers.
            break;
        }

//        System.out.println("expectedHeaders: " + iterateStringArray(HEADERS));
//        System.out.println("actualHeaders: " + iterateStringArray(actualHeaders));
        System.out.println("isOrderOfHeadersCorrectInCsvFile: " + isOrderOfHeadersCorrectInCsvFile);

        // if Order Of Headers Is NOT Correct In Csv File, then provide error details.
        if (!isOrderOfHeadersCorrectInCsvFile) {
            String invalidOrderOfHeadersMessage = "Mismatch in 'Expected' versus 'Actual' fields/columns/headers in csv file." +
                    " Expected headers " + iterateStringArray(IDOMOOHEADERS) + ". But Got Actual headers " + iterateStringArray(actualHeaders);
            errorLinkedHashMap.put(1, invalidOrderOfHeadersMessage);
        }

        return isOrderOfHeadersCorrectInCsvFile;
    }

    public String iterateStringArray(String[] array) {
        String output = "[";
        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1) {
                output += array[i];
            }
            else {
                output += array[i] + ", ";
            }
        }
        output += "]";
        return output;
    }

    public boolean isRandomCsvValidationRequired() {
        return isRandomCsvValidationRequired;
    }
    
    public void givenCSVFile_randomValidateContents() throws IOException {
    	 props = readPropertyFile.ReadCirpTVPropertyFile();
    	 String purlFile = props.getProperty("purlFile");
        Reader in = new FileReader(purlFile);
        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.DEFAULT
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int rowCounter = 2;
        HashSet<Integer> randomRowNumbersSet = getRandomRowNumbers(purlFile, stepPercentageToValidate, percentageOfRowsToValidatePerStep);
        try {
            for (CSVRecord record : records) {
                if (randomRowNumbersSet.contains(rowCounter)) {
                    for (String column : HEADERS) {
                        String columnValue = record.get(column);
//                        System.out.print(columnValue + " , ");
                        validateValuesInCsvFile(rowCounter, column, columnValue);
                    }
                }
                rowCounter++;
//                System.out.println();
            }
        }
        catch (Exception e) {
            errorLinkedHashMap.put(1, "Mismatch in 'Expected' versus 'Actual' fields/columns/headers in csv file.");
        }
    }
    
    public int getCsvFileSizeInTermsOfRows(String purlFile) {
        int csvFileSizeInTermsOfRows = 0;
        try {
            Reader in = new FileReader(purlFile);
            csvFileSizeInTermsOfRows = CSVFormat.DEFAULT
                    .parse(in).getRecords().size();
            System.out.println("________Size of CSV :_________________: "+ csvFileSizeInTermsOfRows);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return csvFileSizeInTermsOfRows;
    }

    public HashSet<Integer> getRandomRowNumbers(String purlFile, double stepPercentageToValidate,double percentageOfRowsToValidatePerStep) {
        int csvFileSizeInTermsOfRows = getCsvFileSizeInTermsOfRows(purlFile);
        int numberOfRowsPerStep = (int) (csvFileSizeInTermsOfRows * (stepPercentageToValidate / 100));
        int numberOfRowsToValidatePerStep = (int) (numberOfRowsPerStep * (percentageOfRowsToValidatePerStep / 100));
        int numberOfSteps = (int) Math.ceil((double) csvFileSizeInTermsOfRows / numberOfRowsPerStep);
        // Exclude getting rowNumber for headers. We want rowNumbers for actual row data only.
        int startingRowIndex = 2;
        HashSet<Integer> randomRowNumbersSet = new HashSet<>();

        int stepCounter = 1;
        int rowNumber;
        while (startingRowIndex < csvFileSizeInTermsOfRows) {
            int numberOfUniqueRandomRowsPerStep = 0;
            // If the numberOfRowsToValidatePerStep is zero,
            // OR the last step has lesser number of rows than numberOfRowsToValidatePerStep,
            // then include all rows that are remaining and break the loop.
            if ((numberOfRowsToValidatePerStep == 0) || (stepCounter == numberOfSteps) && (csvFileSizeInTermsOfRows - startingRowIndex < numberOfRowsToValidatePerStep)) {
                for (int i = startingRowIndex; i <= csvFileSizeInTermsOfRows; i++) {
                    randomRowNumbersSet.add(i);
                }
                break;
            }
            while (numberOfUniqueRandomRowsPerStep < numberOfRowsToValidatePerStep) {
                rowNumber = (int) (startingRowIndex + (Math.random() * numberOfRowsPerStep));
                if (rowNumber < csvFileSizeInTermsOfRows && !randomRowNumbersSet.contains(rowNumber)) {
                    randomRowNumbersSet.add(rowNumber);
                    numberOfUniqueRandomRowsPerStep++;
                }
            }
            startingRowIndex += numberOfRowsPerStep;
            stepCounter++;
        }

//        System.out.println("randomRowNumberSet: ");
        for (int randomRowNumber: randomRowNumbersSet) {
//            System.out.print(randomRowNumber + ", ");
        }
//        System.out.println("\n-----------------------------------------");
//        System.out.println("randomRowNumbersSet.size() : " + randomRowNumbersSet.size());
        return randomRowNumbersSet;
    }
 
    public void givenCSVFile_whenRead_thenContentsAsExpected() throws IOException {
    	 props = readPropertyFile.ReadCirpTVPropertyFile();
    	 String purlFile = props.getProperty("purlFile");
        Reader in = new FileReader(purlFile);
        Iterable<CSVRecord> records = null;
        try {
            records = CSVFormat.DEFAULT
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int rowCounter = 2;
        try {
            for (CSVRecord record : records) {
                for (String column : HEADERS) {
                    String columnValue = record.get(column);
//                    System.out.print(columnValue + " , ");
                    validateValuesInCsvFile(rowCounter, column, columnValue);
                }
                rowCounter++;
//                System.out.println();
            }
        }
        catch (Exception e) {
            errorLinkedHashMap.put(1, "Mismatch in 'Expected' versus 'Actual' fields/columns/headers in csv file.");
        }
    }

    public void validateValuesInCsvFile(int rowCounter, String column, String columnValue) {
    	
        switch (column) {
            case "unique_id":
                validateUniqueIdField(rowCounter, column, columnValue);  
               
                break;

            case "name_1":
            case "name_2":
            case "name_3":
            case "name_4":
            case "name_5":
            case "custom_price_1":
            case "custom_price_2":
            case "custom_price_3":
            case "custom_price_4":
            case "custom_price_5":
                validateMandatoryField(rowCounter, column, columnValue);
                break;

            case "qr_code_url":
            case "image_url_1":
            case "image_url_2":
            case "image_url_3":
            case "image_url_4":
            case "image_url_5":
                verifyLink(rowCounter, column, columnValue);
                break;
            case "VIDEO_URL":
            	
        }
    }

    public void validateUniqueIdField(int rowCounter, String column, String columnValue) {
    	
        boolean isUniqueIdValid = false;
        boolean isUniqueIdPresent = false;
        // maid format : uppercase 32 hex numbers, generally presented as 8-4-4-4-12
        if (Pattern.matches("[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}", columnValue)) 
        {
            isUniqueIdValid = true;
        }

        System.out.println("is UniqueId " + columnValue + " valid? :" + isUniqueIdValid);
       
        // when unique_id is NOT valid
        if (!isUniqueIdValid) {
            String uniqueIdInvalidMessage = " \"" + column + "=" + columnValue + "\" " + "is an invalid DID. DID does NOT follow maid format and/or is NOT in uppercase;";
            errorLinkedHashMap.put(rowCounter, errorLinkedHashMap.getOrDefault(rowCounter, "") + uniqueIdInvalidMessage);
        }
        
        //To validate no duplicate Unique ID
        validateDuplicateUniqueId(rowCounter, column, columnValue);
       
    }


    public void validateMandatoryField(int rowCounter, String column, String columnValue) {
    	
        if (columnValue.isEmpty()) {
            String emptyCellMessage = " \"" + column + "=" + columnValue + "\" " + "is an empty cell;";
            errorLinkedHashMap.put(rowCounter, errorLinkedHashMap.getOrDefault(rowCounter, "") + emptyCellMessage);
        }
    }

    public void verifyLink(int rowCounter, String column, String columnValue) {
    	
        boolean isLinkBroken = false;
        try {
            URL url = new URL(columnValue);
            //Now we will be creating url connection and getting the response code
            HttpURLConnection httpURLConnect = (HttpURLConnection) url.openConnection();
            httpURLConnect.setConnectTimeout(5000);
            httpURLConnect.connect();
            if (httpURLConnect.getResponseCode() >= 400) {
//                System.out.println(columnValue + " - " + httpURLConnect.getResponseMessage() + "is a broken link.");
                isLinkBroken = true;
            }
            //Fetching and Printing the response code obtained
            else {
//                System.out.println("\n" + columnValue + " has status code - " + httpURLConnect.getResponseMessage());
            }
        } catch (Exception e) {
            System.out.println("Exception in verifyLink() method: " + e);
            isLinkBroken = true;
        }
        if (isLinkBroken) {
            String brokenLinkMessage = " \"" + column + "=" + columnValue + "\" " + "is a broken link;";
            errorLinkedHashMap.put(rowCounter, errorLinkedHashMap.getOrDefault(rowCounter, "") + brokenLinkMessage);
//            System.out.println(brokenLinkMessage);
        }
    }

    public void getErrorsInCsvFile(String errorFile) {
//        System.out.println("\nSize of Error File: " + errorLinkedHashMap.size());
        for (Map.Entry item : errorLinkedHashMap.entrySet()) {
            String errorDetails = "Affected Row Number: " + item.getKey() + " : " + item.getValue();
            System.out.println(errorDetails);
            try {
                storeErrorDetailsInCsvFile(errorFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void storeErrorDetailsInCsvFile(String errorFile) throws IOException {

        FileWriter out = new FileWriter(errorFile);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader("Affected Row Number", "ERROR DETAILS"))) {
            errorLinkedHashMap.forEach((affectedRowNumber, errorDetails) -> {
                try {
                    printer.printRecord(affectedRowNumber, errorDetails);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }
    public void validateDuplicateUniqueId(int rowCounter, String column, String columnValue) {    	
    	uniqueIDLinkedHashMap.put(columnValue,uniqueIDLinkedHashMap.getOrDefault(columnValue, 0)+1);
    	int defaultValue=(uniqueIDLinkedHashMap.getOrDefault(columnValue, 0));
    	 if (defaultValue>1) 
    	 {
             String uniqueIdDuplicateValueMessage = " \"" + column + "=" + columnValue + "\" " + "is an duplicate DID.";
             errorLinkedHashMap.put(rowCounter, errorLinkedHashMap.getOrDefault(rowCounter, "") + uniqueIdDuplicateValueMessage);
         }
    	
}
    
 
}

