package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TestPlan {
	
	int id;
	String name;
	Team team;
	ResponsibleTeam responsibleTeam;
	Project project;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TestPlan(int id) {
		this.id = id;
	}

	public TestPlan() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TestPlan(String name) {
		this.name = name;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public ResponsibleTeam getResponsibleTeam() {
		return responsibleTeam;
	}

	public void setResponsibleTeam(ResponsibleTeam responsibleTeam) {
		this.responsibleTeam = responsibleTeam;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	

}
