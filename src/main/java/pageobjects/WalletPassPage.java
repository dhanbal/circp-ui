package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;
import utils.WebElementHandler;

public class WalletPassPage {
    public static final String rotateYourWalletPassDivText = "ROTATE YOUR WALLET PASS TO SEE YOUR DEALS";
    public static final String clickTheLinkOnTheBackDivText = "CLICK THE LINK ON THE BACK OF THIS WALLET PASS";
    public static final String passInfoTextDiv1Text = "Scan the QR Code using a scanner app on your smartphone";
    public static final String downloadPassButtonText  = "I have Android Pay";
    public static final String passInfoTextDiv2Text  = "For a better experience open this page on your smartphone";

    WebDriver driver;
    SoftAssert softAssert;
//    ObjectActions oa;

    @FindBy (xpath="//div[@class='field']//div[@class='label']")
    public WebElement rotateYourWalletPassDiv;

    @FindBy (xpath="//div[@class='field']//div[@class='value']")
    public WebElement clickTheLinkOnTheBackDiv;

    @FindBy (xpath="//div[@class='p-download__pass-info']//div[@class='p-download__pass-txt'][1]")
    public WebElement passInfoTextDiv1;

    @FindBy (xpath="//img[@class='img-responsive' and @alt='QR Code']")
    public WebElement QRCodeDiv;

    @FindBy (xpath="//a[@href='#']")
    public WebElement downloadPassButton;

    @FindBy (xpath="//div[@class='p-download__pass-info']//div[@class='p-download__pass-txt'][2]")
    public WebElement passInfoTextDiv2;

    public WalletPassPage(WebDriver driver, SoftAssert softAssert) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
        this.softAssert = softAssert;
    }

    public String getQRCodeURL() {
        String qrCodeURL = QRCodeDiv.getAttribute("src");
        System.out.println("qrCodeURL: " + qrCodeURL);
        return qrCodeURL;
    }

    public boolean verifyPresenceAndContentOfElementsOnWalletPassPage() {
        boolean areElementsAndContentPresent = false;

        boolean isRotateYourWalletPassDivDisplayed = WebElementHandler.isWebElementDisplayed(driver, rotateYourWalletPassDiv);
        System.out.println("isRotateYourWalletPassDivDisplayed: " + isRotateYourWalletPassDivDisplayed);

        boolean isClickTheLinkOnTheBackDivDisplayed = WebElementHandler.isWebElementDisplayed(driver, clickTheLinkOnTheBackDiv);
        System.out.println("isClickTheLinkOnTheBackDivDisplayed: " + isClickTheLinkOnTheBackDivDisplayed);

        boolean isPassInfoTextDiv1Displayed = WebElementHandler.isWebElementDisplayed(driver, passInfoTextDiv1);
        System.out.println("isPassInfoTextDiv1Displayed: " + isPassInfoTextDiv1Displayed);

        boolean isQRCodeDivDisplayed = WebElementHandler.isWebElementDisplayed(driver, QRCodeDiv);
        System.out.println("isQRCodeDivDisplayed: " + isQRCodeDivDisplayed);

        boolean isDownloadPassButtonDisplayed =  WebElementHandler.isWebElementDisplayed(driver, downloadPassButton);
        System.out.println("isDownloadPassButtonDisplayed: " + isDownloadPassButtonDisplayed);

        boolean isPassInfoTextDiv2Displayed = WebElementHandler.isWebElementDisplayed(driver, passInfoTextDiv2);
        System.out.println("isPassInfoTextDiv2Displayed: " + isPassInfoTextDiv2Displayed);

        if (isRotateYourWalletPassDivDisplayed && isClickTheLinkOnTheBackDivDisplayed && isPassInfoTextDiv1Displayed &&
                isQRCodeDivDisplayed && isDownloadPassButtonDisplayed && isPassInfoTextDiv2Displayed) {
//            Check content of elements
            if (rotateYourWalletPassDivText.equals(rotateYourWalletPassDiv.getText()) &&
                    clickTheLinkOnTheBackDivText.equals(clickTheLinkOnTheBackDiv.getText()) &&
                    passInfoTextDiv1Text.equals(passInfoTextDiv1.getText()) &&
                    downloadPassButtonText.equals(downloadPassButton.getText()) &&
                    passInfoTextDiv2Text.equals(passInfoTextDiv2.getText())) {
                System.out.println(rotateYourWalletPassDiv.getText());
                System.out.println(clickTheLinkOnTheBackDiv.getText());
                System.out.println(passInfoTextDiv1.getText());
                if (isDownloadPassButtonDisplayed) {
                    System.out.println(downloadPassButton.getText());
                    areElementsAndContentPresent = true;
                }
                System.out.println(passInfoTextDiv2.getText());
            }
        }

        return areElementsAndContentPresent;
    }

}
