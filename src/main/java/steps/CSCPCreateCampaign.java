package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageobjects.AfterLoginPage;
import pageobjects.CreateCampaign;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CSCPCreateCampaign {
    WebDriver driver=CSCPLogin.driver;
    WebDriverWait wait=new WebDriverWait(driver,10);
    AfterLoginPage afterLoginPage=new AfterLoginPage(driver);
    CreateCampaign createCampaign=new CreateCampaign(driver);



    @Given("^Go to Campaign Page$")
    public void go_to_Campaign_Page() {
        try {

            System.out.println("Now will perform Campaign action");
            wait.until(ExpectedConditions.elementToBeClickable(afterLoginPage.getNav_bar()));
            afterLoginPage.getNav_bar().click();
            wait.until(ExpectedConditions.visibilityOf(afterLoginPage.getCreateCampaign()));
            wait.until(ExpectedConditions.elementToBeClickable(afterLoginPage.getCreateCampaign()));
            Thread.sleep(2*1000);
            afterLoginPage.getCreateCampaign().click();
        }
        catch (ElementNotVisibleException enve){

            System.out.println("Error :" +enve.getMessage());
            enve.printStackTrace();
            Assert.fail();

        }
        catch (NoSuchElementException nsee){
            nsee.printStackTrace();
            Assert.fail();
        }
        catch (Exception e){

            e.printStackTrace();
            Assert.fail();
        }


    }


    @Then("^Validate Budget Field Name$")
    public void validate_Budget_Field_Name() throws Throwable {
        try {
            System.out.println("Validate the budget field");
            String budgetField = createCampaign.getBudgetField().getText();
            boolean budgetStatus=budgetField.contains("Sponsored Ad Budget Amount");
            Assert.assertTrue(budgetStatus);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Then("^Create Campaign \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void create_Campaign(String retName, String cgName, String brand, String flowType, String adWeeStart, String noOfWeek, String budget, String upc) throws Throwable {
      //  Thread.sleep(5*1000);
        createCampaign.getCreateCampaignIcon().click();
       // Thread.sleep(5*1000);
        //driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[1]/select-configured-network/select-dropdown/span/span[1]/span/span[2]")).click();

        createCampaign.getCreateCampaignDropdown().click();
        //Retailer id to be fetched from Page Object

        driver.findElement(By.xpath("//*[text()='2000 - Fuel Stations']")).click();
        Thread.sleep(2*1000);
        if(cgName.equalsIgnoreCase("DefaultCampaign")) {
            createCampaign.getCampagin_save().click();
            Thread.sleep(3 * 1000);
            String[] errorMessage = new String[]{"Missing Advertiser/Brand Name.", "Missing Campaign Name.", "Missing Budget Amount.", "Missing UPC's.", "Missing Start Date.",
                    "Missing Stop Date.", "Missing Campaign Type."};

            boolean status = false;
            int matchCount=0;
            for (int i = 0; i < errorMessage.length; i++) {
                // for (int j = 1; j <= 7; j++) {
                String elementIs = driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[2]/div/ul/li[" + (i + 1) + "]")).getText();
                System.out.println("The error message is " + elementIs + errorMessage[i]);
                status = errorMessage[i].equalsIgnoreCase(elementIs);
                if(status)
                {
                    matchCount++;
                }
            }
            Assert.assertEquals(matchCount,errorMessage.length);
        }
          createCampaign.getCampaignName().sendKeys(cgName);
          createCampaign.getAdvertiserName() .sendKeys(brand);
        System.out.println("Current campaign before selecting "+createCampaign.getCurrentCampaignType().getText());
        createCampaign.getFlowTypeDropDown().click();
        createCampaign.getKnownFlow().click();
        System.out.println("Current campaign after k selecting "+createCampaign.getCurrentCampaignType().getText());
        createCampaign.getFlowTypeDropDown().click();
        createCampaign.getUnknownFlow().click();

        System.out.println("Current campaign after u selecting "+createCampaign.getCurrentCampaignType().getText());

        Thread.sleep(1000*3);
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span")).sendKeys("KNOWN");

        createCampaign.getAdWeekStart().sendKeys(adWeeStart);
        createCampaign.getNoOfAdWeek().sendKeys(noOfWeek);;
        createCampaign.getAdBudget().sendKeys(budget);
        createCampaign.getUpc().sendKeys(upc);
        createCampaign.getUpc().click();
        Thread.sleep(5*1000);

        //driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[1]")).click();

    }

    @Then("^Create Campaign with empty values \"([^\"]*)\"$")
    public void create_Campaign_with_empty_values(String retailer) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        try {
            wait.until(ExpectedConditions.visibilityOf(createCampaign.getCreateCampaignIcon()));
            wait.until(ExpectedConditions.elementToBeClickable(createCampaign.getCreateCampaignIcon()));
            Thread.sleep(3*1000);
            createCampaign.getCreateCampaignIcon().click();
            wait.until(ExpectedConditions.visibilityOf(createCampaign.getCreateCampaignDropdown()));
            wait.until(ExpectedConditions.elementToBeClickable(createCampaign.getCreateCampaignDropdown()));
            createCampaign.getCreateCampaignDropdown().click();
            //Retailer id to be fetched from Page Object

            driver.findElement(By.xpath("//*[text()='"+retailer+"']")).click();
            Thread.sleep(2*1000);
        }
        catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Then("^Validate error messages for mandatory fields$")
    public void validate_error_messages_for_mandatory_fields() {
        try {
                createCampaign.getCampagin_save().click();
                Thread.sleep(3 * 1000);
                String[] errorMessage = new String[]{"Missing Advertiser/Brand Name.", "Missing Campaign Name.", "Missing Budget Amount.", "Missing UPC's.", "Missing Start Date.",
                        "Missing Stop Date.", "Missing Campaign Type."};

                boolean status = false;
                int matchCount=0;
                for (int i = 0; i < errorMessage.length; i++) {
                    String elementIs = driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[2]/div/ul/li[" + (i + 1) + "]")).getText();
                    System.out.println("Actual error message is " + elementIs +"Expected error "+ errorMessage[i]);
                    status = errorMessage[i].equalsIgnoreCase(elementIs);
                    if(status)
                    {
                        matchCount++;
                    }
                }
                Assert.assertEquals(matchCount,errorMessage.length);
            }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail();
        }

    }

    @Then("^Create Campaign with date range of (\\d+) days \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void create_Campaign_with_date_range_of_days(int days, String retName, String priority,String cgName, String brand, String flowType, String noOfWeek, String budget, String upc) throws Throwable {

        SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/YYYY");
        try {
                Calendar cal=Calendar.getInstance();
                cal.add(Calendar.DATE,days);
                Date date=cal.getTime();
                String adWeekStart=sdf.format(date);
            Thread.sleep(3*1000);
            wait.until(ExpectedConditions.visibilityOf(createCampaign.getCreateCampaignIcon()));
            wait.until(ExpectedConditions.elementToBeClickable(createCampaign.getCreateCampaignIcon()));
            createCampaign.getCreateCampaignIcon().click();
            wait.until(ExpectedConditions.visibilityOf(createCampaign.getCreateCampaignDropdown()));
            createCampaign.getCreateCampaignDropdown().click();
            //Retailer id to be fetched from Page Object

            driver.findElement(By.xpath("//*[text()='"+retName+"']")).click();
            createCampaign.getNoOfAdWeek().click();
            createCampaign.getCampaignName().sendKeys(cgName);
            createCampaign.getAdvertiserName() .sendKeys(brand);
            System.out.println("Current campaign before selecting "+createCampaign.getCurrentCampaignType().getText());
            createCampaign.getFlowTypeDropDown().click();
           if(flowType.equalsIgnoreCase("KNOWN")) {
               createCampaign.getKnownFlow().click();
               System.out.println("Current campaign after k selecting " + createCampaign.getCurrentCampaignType().getText());
           }
           if(flowType.equalsIgnoreCase("UNKNOWN")) {
               createCampaign.getUnknownFlow().click();
               System.out.println("Current campaign after u selecting " + createCampaign.getCurrentCampaignType().getText());
           }

           createCampaign.getAdSlotPriorityDrowDown().click();
           if(priority.equalsIgnoreCase("High"))
           {
               createCampaign.getAdSlotPriorityHigh().click();
           }
           if(priority.equalsIgnoreCase("low"))
           {
               createCampaign.getAdSlotPriorityLow().click();
           }
            createCampaign.getAdWeekStart().sendKeys(adWeekStart);
            createCampaign.getNoOfAdWeek().sendKeys(noOfWeek);;
            createCampaign.getAdBudget().sendKeys(budget);
            createCampaign.getUpc().sendKeys(upc);
            createCampaign.getUpc().click();
        }
        catch (ElementNotVisibleException enve){

            System.out.println("Error :" +enve.getMessage());
            enve.printStackTrace();
            Assert.fail();

        }
        catch (NoSuchElementException nsee){
            nsee.printStackTrace();
            Assert.fail();
        }
        catch (Exception e){

            e.printStackTrace();
            Assert.fail();
        }

    }

    @Then("^Validate DB \"([^\"]*)\"$")
    public void validate_DB(String arg1) throws Throwable {

     System.out.println("Call DB query and get result");

    }

    //@Then("^Create Campaign with date range less or equal (\\d+) days \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
//    public void create_Campaign_with_date_range_less_or_equal_days(int arg1, String retName, String priority, String cgName, String brand, String flowType, String noOfWeek, String budget, String upc) throws Throwable {
//        SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/YYYY");
//        try {
//            Calendar cal=Calendar.getInstance();
//            cal.add(Calendar.DATE,6);
//            Date date=cal.getTime();
//            String adWeekStart=sdf.format(date);
//            System.out.println("Date input is "+adWeekStart);
//
//            wait.until(ExpectedConditions.elementToBeClickable(createCampaign.getCreateCampaignIcon()));
//            createCampaign.getCreateCampaignIcon().click();
//            wait.until(ExpectedConditions.visibilityOf(createCampaign.getCreateCampaignDropdown()));
//            createCampaign.getCreateCampaignDropdown().click();
//            driver.findElement(By.xpath("//*[text()='2000 - Fuel Stations']")).click();
//            createCampaign.getNoOfAdWeek().click();
//            createCampaign.getCampaignName().sendKeys(cgName);
//            createCampaign.getAdvertiserName() .sendKeys(brand);
//            System.out.println("Current campaign before selecting "+createCampaign.getCurrentCampaignType().getText());
//            createCampaign.getFlowTypeDropDown().click();
//            if(flowType.equalsIgnoreCase("KNOWN")) {
//                createCampaign.getKnownFlow().click();
//                System.out.println("Current campaign after k selecting " + createCampaign.getCurrentCampaignType().getText());
//            }
//            if(flowType.equalsIgnoreCase("UNKNOWN")) {
//                createCampaign.getFlowTypeDropDown().click();
//                createCampaign.getUnknownFlow().click();
//                System.out.println("Current campaign after u selecting " + createCampaign.getCurrentCampaignType().getText());
//            }
//            createCampaign.getAdSlotPriorityDrowDown().click();
//            if(priority.equalsIgnoreCase("High"))
//            {
//                createCampaign.getAdSlotPriorityHigh().click();
//            }
//            if(priority.equalsIgnoreCase("low"))
//            {
//                createCampaign.getAdSlotPriorityLow().click();
//            }
//            createCampaign.getAdWeekStart().sendKeys(adWeekStart);
//            createCampaign.getNoOfAdWeek().sendKeys(noOfWeek);;
//            createCampaign.getAdBudget().sendKeys(budget);
//            createCampaign.getUpc().sendKeys(upc);
//            createCampaign.getUpc().click();
//
//        }
//        catch (ElementNotVisibleException enve){
//            enve.printStackTrace();
//            Assert.fail();
//
//        }
//        catch (NoSuchElementException nsee){
//            nsee.printStackTrace();
//            Assert.fail();
//        }
//        catch (Exception e){
//
//            e.printStackTrace();
//            Assert.fail();
//        }
//
//    }

    @Then("^Validate error message \"([^\"]*)\"$")
    public void validate_error_message(String ExpectedResult) throws Throwable {
        boolean warning=true;
        try {

            if(ExpectedResult.contains("Warning")){
//                String errorMessageWithWrongDATE="Modifications is not allowed if Start Date less then 7 days from current date";
                createCampaign.getCampagin_save().click();
                String output=createCampaign.getErrorMessageWithWrongDate().getText();
                System.out.println("Message is "+output);
                warning=output.contains(ExpectedResult.split("-")[1]);
            }
            else{
                System.out.println("Message is "+ExpectedResult.split("-")[1]);
                //As not we will not save
//                createCampaign.getCampagin_save().click();
//                String success=driver.findElement(By.xpath("/html/body/div[4]/div/div[1]")).getText();
//                warning=success.contains(ExpectedResult);
            }


            Assert.assertTrue(warning);

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();

        }
    }
}
