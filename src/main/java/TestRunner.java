//import io.cucumber.junit.Cucumber;
//import io.cucumber.junit.CucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Users\\csauto4\\Desktop\\Catalina-WS\\circp-ui\\src\\main\\java\\features",
        glue = "steps",
        tags = "@cscp"
)
public class TestRunner {
}
