package steps;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


public class Demo {

    public static final String OMNIMFD = "omnimfd";
        public static final String SHOPLIFTR_PROMOTION = "shopliftrPromotion";

        public static void main(String args[]) {

            String mongoUrl ="mongodb://mta:y38ngBDNHWmFgLE@sqa-1-shard-00-00-tbdu2.azure.mongodb.net:27017,sqa-1-shard-00-01-tbdu2.azure.mongodb.net:27017,sqa-1-shard-00-02-tbdu2.azure.mongodb.net:27017/admin.omnimfd&authMechanism=SCRAM-SHA-1&replicaSet=atlas-sqa&readPreference=primary&connectTimeoutms=6000&socketTimeoutMS=300000?tls=true";
                    //"mongodb+srv://mta:y38ngBDNHWmFgLE@sqa-1-tbdu2.azure.mongodb.net/omnimfd";

            MongoClientURI mongoClientURI = new MongoClientURI(mongoUrl);
            try (MongoClient mongoClient = new MongoClient(mongoClientURI)) {

                MongoDatabase db = mongoClient.getDatabase(OMNIMFD);
                System.out.println("Database is connected");

                // list of all collections
                System.out.println("Database collection listing ....");
                for (String name : db.listCollectionNames()) {
                    System.out.println(">>> " + name);
                }
                System.out.println("--------------------------------------------------");

                // Fetching the collection from the mongodb
                MongoCollection<Document> coll = db.getCollection(SHOPLIFTR_PROMOTION);

                // find ALL documents
                //List<Document> foundDocument = coll.find().into(new ArrayList<Document>()); returns all records ....

                // db.getCollection('shopliftrPromotion').find({"retailerId": "24", "upc" : "2548400732" })
                BasicDBObject query = new BasicDBObject();
                query.put("retailerId", "24");
                query.put("upc", "2548400732");

                FindIterable<Document> fi = coll.find(query);
                fi.limit(10);
                MongoCursor<Document> cursor = fi.iterator();
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data is: " + data);
                }
                cursor.close();

                mongoClient.close();
                System.out.println("Connection closed");
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

