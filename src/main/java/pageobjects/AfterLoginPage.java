package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.util.concurrent.TimeUnit;

public class AfterLoginPage {

    WebDriver driver;
    public AfterLoginPage(WebDriver driver) {
        this.driver = driver;
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(this.driver, this);

    }

    @FindBy(xpath = "//*[@id=\"hamburger-button-toggle\"]/div[2]")
    WebElement nav_bar;

    @FindBy(xpath = "//*[@id=\"sidebar\"]/div/ul/li[1]/a")
    WebElement createRetailer;

    @FindBy(xpath = "//*[@id=\"sidebar\"]/div/ul/li[2]/a")
    WebElement createCampaign;

    public WebElement getCreateRetailer() {
        return createRetailer;
    }

    public WebElement getCreateCampaign() {
        return createCampaign;
    }

    public WebElement getNav_bar() {
        return nav_bar;
    }

    WebElement paddingButton=null;

}
