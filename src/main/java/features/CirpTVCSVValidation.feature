Feature: CirpTV Flow
  #This Feature is to validate below test cases

#  Completed
  @CirpTVcsv
  Scenario Outline: Verify idoomo csv file for CirpTV flow.
#    Given Excel file is saved to a location in a proper format "<csvFilePath>" "<filename>" "<countryCode>" "<networkId>" "<flowType>"
    Then Validate excel field values against shopLiftrUserOffer DB "<filename>" "<networkId>" "<identifier>" "<flowType>"


    Examples:
      | countryCode | networkId |  flowType  |retailerName   | csvFilePath  |filename                   |identifier|
      |    USA      |     24   |  TVKN      |Meijer         | C:\Users\csauto4\Downloads\   |idomoo-USA6.1TVKN20211220_1.csv|USA-0024-100000187|


#  "<unique_code>" "<qr-code-url>" "<name>" "<additional>" "<image_url>" "<custom_price>" "<brand>"