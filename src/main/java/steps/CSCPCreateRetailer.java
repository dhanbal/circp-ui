package steps;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.eo.Se;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageobjects.AfterLoginPage;
import pageobjects.CreateRetailer;
//import sun.rmi.runtime.Log;

import java.io.EOFException;
import java.util.logging.Logger;

public class CSCPCreateRetailer {
    WebDriver driver=CSCPLogin.driver;
    WebDriverWait wait=new WebDriverWait(driver,10);
    WebElement nav_bar;
    AfterLoginPage afterLoginPage=new AfterLoginPage(driver);
    CreateRetailer createRetailer=new CreateRetailer(driver);
    static Logger logger=Logger.getLogger(String.valueOf(CSCPCreateRetailer.class));

    @Given("^Enter Retailer Name \"([^\"]*)\"$")
    public void enter_Retailer_Name(String retailerName  )  {

        try {
            wait.until(ExpectedConditions.elementToBeClickable(afterLoginPage.getNav_bar()));
            afterLoginPage.getNav_bar().click();
            wait.until(ExpectedConditions.visibilityOf(afterLoginPage.getCreateRetailer()));
            wait.until(ExpectedConditions.elementToBeClickable(afterLoginPage.getCreateRetailer()));
            afterLoginPage.getCreateRetailer().click();
            wait.until(ExpectedConditions.visibilityOf(createRetailer.getCreateRetailerIcon()));
            Thread.sleep(2*1000);
            wait.until(ExpectedConditions.elementToBeClickable(createRetailer.getCreateRetailerIcon()));
            createRetailer.getCreateRetailerIcon().click();
            if (createRetailer.getSettingIconInCreateRetailer().isDisplayed()) {
                System.out.println("Retailer will be created");
                wait.until(ExpectedConditions.elementToBeClickable(createRetailer.getCreateRetailerDropdown()));
                createRetailer.getCreateRetailerDropdown().click();
                driver.findElement(By.xpath("//*[text()='" + retailerName + "']")).click();
                // createRetailer.getFuelStation().click();

            }
        }
       catch(ElementNotVisibleException enve){
           enve.printStackTrace();
           Assert.fail();
            }
        catch (Exception e){
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Then("^Enter Go_Live Day \"([^\"]*)\"$")
    public void enter_Go_Live_Day(String liveDay) {
        try {

            System.out.println("Day is "+liveDay);
            createRetailer.setGoliveDay(liveDay);
            System.out.println("Day is "+createRetailer.getGoliveDay().getText());
            createRetailer.getGoliveDay().click();
        }
        catch (Exception e){
            e.printStackTrace();
            Assert.fail("Failed to set Go live Day"+liveDay);
        }

    }


    @Then("^Select Priority \"([^\"]*)\" \"([^\"]*)\"$")
    public void select_Priority(String priority ,String slots) throws Throwable {

        try {

        //clearing pre exist value in add slots(default is 5)
        createRetailer.getNoOfAdSlots().clear();
        createRetailer.getNoOfAdSlots().sendKeys(slots);
      int  slotNo=Integer.parseInt(slots);
        Thread.sleep(2*1000);
        String expected1="Sponsored High";
        String expected2="Sponsored Low";
        if(slotNo==1) {
            System.out.println("No Task to be performed");
            //For single slot High must be selected
            System.out.println("Since No of Slot is " +
                    ""+slotNo+" the auto selected priority is " +
                    ""+createRetailer.getSponsoredHigh().getText());
            Assert.assertEquals(createRetailer.getSponsoredHigh().getText(),expected1);
        }
        if(slotNo==2) {
            createRetailer.getSlot1().click();
            createRetailer.getHighPriority1().click();
            //click on High
            //clck on low
            createRetailer.getSlot2().click();
            createRetailer.getLowPriority2().click();
            Assert.assertEquals(createRetailer.getSponsoredHigh().getText(),expected1);
            Assert.assertEquals(createRetailer.getSponsoredLow().getText(),expected2);

        }
        if(slotNo==3) {
            createRetailer.getSlot1().click();
            //click on High
            //clck on low
            createRetailer.getSlot2().click();
        }
        if(slotNo==4) {
            createRetailer.getSlot1().click();
            //click on High
            //clck on low
            createRetailer.getSlot2().click();
        }
        if(slotNo==5) {
            createRetailer.getSlot1().click();
            //click on High
            //clck on low
            createRetailer.getSlot2().click();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Then("^cancel$")
    public void cancel() throws Throwable {
        //driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[1]")).click();
        createRetailer.getRetailerPageCancel().click();
        Thread.sleep(2*1000);
//
////        System.out.println("Now will perform Campaign action");
////        afterLoginPage.getNav_bar().click();
////        Thread.sleep(2*1000);
////        driver.findElement(By.xpath("//*[@id=\"sidebar\"]/div/ul/li[2]/a")).click();
//        Thread.sleep(5*1000);
//        driver.findElement(By.xpath("//*[@id=\"services\"]/div[2]/a/i")).click();
//        Thread.sleep(5*1000);
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[1]/select-configured-network/select-dropdown/span/span[1]/span/span[2]")).click();
//        driver.findElement(By.xpath("//*[text()='2000 - Fuel Stations']")).click();
//        Thread.sleep(2*1000);
//        driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[2]")).click();
//        Thread.sleep(3*1000);
//        String[] errorMessage= new String[]{"Missing Advertiser/Brand Name.", "Missing Campaign Name.","Missing Budget Amount.","Missing UPC's.","Missing Start Date.",
//                "Missing Stop Date.","Missing Campaign Type."};
//
//        boolean status=false;
//        for (int i=0;i<errorMessage.length;i++) {
//            // for (int j = 1; j <= 7; j++) {
//            String elementIs=driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[2]/div/ul/li["+(i+1)+"]")).getText();
//            System.out.println("The error message is "+elementIs+errorMessage[i]);
//            status= errorMessage[i].equalsIgnoreCase(elementIs);
//        }
//
//
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[2]/input")).sendKeys("Auto test Campaign");
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/input")).sendKeys("Auto Test Brand");
////        Select flowT=new Select(driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span")));
////        flowT.selectByVisibleText("KNOWN");
//
//      driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span")).click();
//////*[@id="uk-switch-place"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span/span[2]
//        driver.findElement(By.xpath("//*[text()='KNOWN']")).click();
//        Thread.sleep(2*1000*60);
////        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span")).sendKeys("KNOWN");
//
////Enter Budget
//
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[5]/div/div[2]/date-picker/div/input")).sendKeys("03-09-2022");
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[5]/div/div[3]/input")).sendKeys("2");
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[6]/input")).sendKeys("10000");
//        driver.findElement(By.xpath("//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[7]/textarea")).sendKeys("123123123");
//        Thread.sleep(5*60*1000);
//        //driver.findElement(By.xpath("//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[1]")).click();
//

       // throw new Exception();
    }


}
