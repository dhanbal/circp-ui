package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.jcraft.jsch.*;
 
  public class SFTPClient {
  
      private Session session = null;
      private String host = "sftp-in-us.idomoo.com";
      private int port = 22;
      private String username = "catalinausr2";
      private String password = "HGxonbEejhtS";
      File file;
      FileInputStream fileinput;
      Properties prop;
      String rootPath;
      ReadPropertyFile readPropertyFile;

      
      private ChannelSftp setupJsch() throws JSchException {
    	    JSch jsch = new JSch();
    	     session = jsch.getSession(username, host, port);
          session.setPassword(password);
          session.setConfig("StrictHostKeyChecking", "no");
          session.connect();
          System.out.println("Connected to SFTP");
          return (ChannelSftp) session.openChannel("sftp");
          
    	}
      
      public Properties ReadPropertyFile() throws IOException
      {
          rootPath = System.getProperty("user.dir");
          file = new File("C:\\Users\\csauto4\\Desktop\\KarateFramework\\Cirp - UI\\circp-ui\\CirpTV.properties");
          fileinput = null;
          try
          {
              fileinput = new FileInputStream(file);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }
          prop = new Properties();
          prop.load(fileinput);
          return prop;
          
      }
     
      public void download() throws JSchException, SftpException, IOException {
    	  ChannelSftp channelSftp = setupJsch();
    	  channelSftp.connect();    	
    	  Properties prop = ReadPropertyFile();
    	  String remoteFile= prop.getProperty("remoteFile");
    	  String localDir = prop.getProperty("localDir");
    	 
    	  channelSftp.get(remoteFile, localDir);    	 
    	  channelSftp.exit();    	  
    	}

      public void disconnect() {
          if (session != null) {
              session.disconnect();
          }
      }      
      
      public static void main(String args[]) throws JSchException, SftpException, IOException
      {
    	  SFTPClient client = new SFTPClient();
    	  client.download();
    	  client.disconnect();
    	  
      }
  }