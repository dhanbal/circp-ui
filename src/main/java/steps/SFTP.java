package steps;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

public class SFTP {
	
	    public static void main(String args[]) {
	        JSch jsch = new JSch();
	        Session session = null;
	        try {
	            session = jsch.getSession("catalinausr", "sftp-usa.idomoo.com", 22);
	            session.setConfig("StrictHostKeyChecking", "no");
	            session.setPassword("NmM2ZjUwNzE1");
	            session.connect();

	            Channel channel = session.openChannel("sftp");
	            channel.connect();
	            ChannelSftp sftpChannel = (ChannelSftp) channel;
	            sftpChannel.get("\\retailer_1\\downloads\\remotefile.csv", "C:\\downloads\\localfile.csv");
	            sftpChannel.exit();
	            session.disconnect();
	        } catch (JSchException e) {
	            e.printStackTrace();  
	        } catch (SftpException e) {
	            e.printStackTrace();
	        }
	    }
	}