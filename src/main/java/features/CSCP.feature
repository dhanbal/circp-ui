@cscp
Feature: CSCP mandatory field validation

  Scenario: Login page validation
    Given Navigate to cscp url
    Then  enter user name and password
    Then login

  Scenario Outline: Create Retailer
    Given  Enter Retailer Name "<retailerName>"
    Then Enter Go_Live Day "<liveDay>"
    Then Select Priority "<NoOfpriority>" "<adSlots>"
    Then cancel

  Examples:
    | retailerName         | adSlots | liveDay  | NoOfpriority |
    | Fuel Stations - 2000 | 2       | Thursday | 2            |

  Scenario Outline: Create Campaign with empty values and validate mandatory fields TestcaseIds 399816,402691,403537,411594
    Given  Go to Campaign Page
    Then Create Campaign with empty values "<retailerName>"
    Then Validate error messages for mandatory fields

    Examples:
      | retailerName |
      | 164 - MURPHY |


  Scenario Outline: Create Campaign with date range TestcaseIds 400282,400284,400285,400286,400287,400288,400289,400290,400291,400292,400293,400740,400741,199497
    Given  Go to Campaign Page
    Then Create Campaign with date range of <days> days "<retailerName>" "<priority>" "<campaignName>" "<brandName>" "<flowType>" "<noOfWeek>" "<budget>" "<upc>"
    Then Validate error message "<message>"
    #Then save
    Examples:
      | retailerName   |  | priority | campaignName     | brandName   | flowType | budget | upc       | noOfWeek | message                    | days |  |
      | 164 - MURPHY   |  | High     | DefaultCampaign  | HelloBrand  | KNOWN    | 12345  | 123123126 | 2        | Success-Successfully Saved | 8    |  |
      | 34 - WALGREENS |  | High     | DefaultCampagin1 | HelloBrand1 | KNOWN    | 12345  | 123123124 | 2        | Success-Successfully Saved | 8    |  |
      | 34 - WALGREENS |  | High     | DefaultCampagin  | HelloBrand  | UNKNOWN  | 12345  | 123123124 | 2        | Success-Successfully Saved | 12   |  |
      | 34 - WALGREENS |  | Low      | DefaultCampagin  | HelloBrand  | KNOWN    | 12345  | 123123124 | 2        | Success-Successfully Saved | 15   |  |
      | 34 - WALGREENS |  | Low      | DefaultCampagin  | HelloBrand  | UNKNOWN  | 12345  | 123123124 | 2        | Success-Successfully Saved | 15   |  |

#    @tpupdate
#    Examples:
#      | retailerName   |  | priority | campaignName   | brandName  | flowType | budget | upc       | noOfWeek | message                    | days |
#      | 34 - WALGREENS |  | Low      | DefaultCampagi | HelloBrand | UNKNOWN  | 12345  | 123123124 | 2        | Success-Successfully Saved | 15   |
# 399811,399812,399815,399814,


    #164 - MURPHY		High	DefaultCampaign	HelloBrand	KNOWN	123456	123123126	2	Warning-Modifications is not allowed if Start Date less then 7 days from current date	5
