package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.CSCPLogin;

import java.util.concurrent.TimeUnit;

public class CSCPLoginPage {

//    WebElement userName=null;
//    WebElement password=null;
//    WebElement loginButton=null;
//  //  @FindBy(xpath = "/html/body/div[2]/div/div/div/form/div[2]/div[1]/input")
//    public CSCPLoginPage(){
//
//        userName=CSCPLogin.driver.findElement(By.xpath("/html/body/div[2]/div/div/div/form/div[2]/div[1]/input"));
//        password=CSCPLogin.driver.findElement(By.xpath("/html/body/div[2]/div/div/div/form/div[3]/input"));
//        loginButton=CSCPLogin.driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div/button"));
// }

    WebDriver driver;
    public CSCPLoginPage(WebDriver driver) {
        this.driver = driver;
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(this.driver, this);

    }
    @FindBy(xpath = "/html/body/div[2]/div/div/div/form/div[2]/div[1]/input")
    WebElement userName;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/form/div[3]/input")
    WebElement password;

    @FindBy(xpath = "/html/body/div[2]/div/div/div/div/div/button")
    WebElement loginButton;


    public WebElement getUserName() {
        return userName;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }
}
