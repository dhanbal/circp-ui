package utils;

import org.json.JSONObject;

import java.io.IOException;

public class FileFormatterJSON {
    public static JSONObject formatJSON(String fileContent) throws IOException {
        fileContent = fileContent.replaceAll("}(\\r|\\n)", "},");
        System.out.println("File Content is" + fileContent);
        String data = "{\"data\":[" + fileContent.substring(0, fileContent.length() - 1) + "]}" +
                "";
        System.out.println("data is" + data);
        JSONObject content = new JSONObject(data);
        return content;
    }
}

