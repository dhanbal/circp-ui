package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Project {
	
	int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Project() {
		// TODO Auto-generated constructor stub
	}

}
