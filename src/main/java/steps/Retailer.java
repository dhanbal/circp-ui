package steps;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import com.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import datasharing.DataContainer;
import pageobjects.RetailerLandingPage;
import pageobjects.WalletPassPage;
import utils.HttpResponse;
import utils.MongoDBUtils;
import utils.ReadCSVFile;
import utils.ReadPropertyFile;
import utils.SharedResource;
import utils.TimeUtils;

public class Retailer {
	static WebDriver driver;
	ReadPropertyFile readPropertyFile;
	RetailerLandingPage retailerLandingPage;

	HttpResponse httpResponse;
	DataContainer dataContainer;
	MongoDBUtils mongoDbUtils;
	WalletPassPage walletPassPage;
	SoftAssert softAssert;
	SharedResource sharedResource;
	String retailerName;

	public Retailer(SharedResource sharedResource) {
		this.sharedResource = new SharedResource();
		this.driver = sharedResource.initializeWebDriver();
		this.softAssert = sharedResource.initializeSoftAssertObject();
//        retailerName = sharedResource.getRetailerNameFromScenarioTagNames();
//        retailerName=ReadPropertyFile.getRetailerName(String countryCode, String networkId, String retailerName)

		retailerLandingPage = new RetailerLandingPage(driver, softAssert);
		walletPassPage = new WalletPassPage(driver, softAssert);
		dataContainer = new DataContainer();
	}

	@Given("^Extract click thru url from celtra call response \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void extract_click_thru_url_from_celtra_call_response(String shopliftrCeltraUrl, String countryCode,
			String networkId, String DID, String retailerName) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
//        retailerName = ReadPropertyFile.getRetailerName(countryCode, networkId, retailerName);
		System.out.println("Retailer Name: " + retailerName);
		httpResponse = new HttpResponse();
		System.out.println("DID: " + DID);
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		String expectedLandingPageUrl = httpResponse.getLandingPageUrlFromCeltraResponse(expectedCeltraUrl);

		dataContainer.setLandingPageUrl(expectedLandingPageUrl);
	}

	@Then("^Navigate to retailer landing page \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void navigate_to_retailer_landing_page(String countryCode, String networkId, String DID, String retailerName)
			throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		retailerName = readPropertyFile.getRetailerName(countryCode, networkId, retailerName);

		String expectedLandingPageUrl = dataContainer.getLandingPageUrl();
		Reporter.addStepLog(
				"Navigating to " + retailerName + " landing page for DID " + DID + " : " + expectedLandingPageUrl);
		driver.navigate().to(expectedLandingPageUrl);
		String actualLandingPageUrl = driver.getCurrentUrl();
		System.out.println("actualLandingPageUrl on browser is: " + actualLandingPageUrl);
		Assert.assertEquals(expectedLandingPageUrl, actualLandingPageUrl);
	}

	@Then("^Verify order of elements on retailer landing page$")
	public void verify_order_of_elements_on_retailer_landing_page() throws Throwable {
		boolean isOrderMatched = retailerLandingPage.verifyOrderOfElementsOnRetailerLandingPage();
		softAssert.assertTrue(isOrderMatched,
				"Order of elements" + (isOrderMatched ? " " : " NOT ") + "matched on retailer landing page");
		sharedResource.addScreenshot(driver);
	}

	@Then("^compare offers details on retailer landing page with shopliftrOffer mongoDB collection \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void compare_offers_details_on_retailer_landing_page_with_shopliftrOffer_mongoDB_collection(
			String shopliftrCeltraUrl, String countryCode, String networkId, String DID) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		String identifier = DID;
		if (expectedCeltraUrl.contains("unknown")) {
			identifier = DID;
		} else {
			httpResponse = new HttpResponse();
			String CID = httpResponse.getCidForDidUsingCardlinkPostCall(countryCode, networkId, DID);
			dataContainer.setCID(CID);
			identifier = CID;
		}

		HashMap<String, String> offersDetailsLinkedHashmapFromLandingPage = retailerLandingPage.getOffersDetailsLinkedHashmapFromLandingPage();
		mongoDbUtils = new MongoDBUtils();
		LinkedHashMap<String, String> result = mongoDbUtils
				.compareOffersDetailsOnLandingPageWithShopliftrOfferMongoDBCollection(
						offersDetailsLinkedHashmapFromLandingPage, expectedCeltraUrl, networkId, identifier);


//        softAssert.assertEquals("MATCHED", result.get("isMatched"), "Offers details on landing page " + result.get("isMatched")+ " with MongoDB");
		Assert.assertEquals("Offers details on landing page " + result.get("isMatched") + " with MongoDB", "MATCHED",
				result.get("isMatched"));
		int offersCountInDb = Integer.parseInt(result.get("offersCountInDb"));
		long minimumOfShopliftrOfferEndDate = Long.parseLong(result.get("minimumOfShopliftrOfferEndDate"));
		dataContainer.setMinimumOfShopliftrOfferEndDate(minimumOfShopliftrOfferEndDate);
		retailerLandingPage.verifyOffersCountOnLandingPageIsEven(offersCountInDb);
	}

	@Then("^Verify deals are available until upcoming minimum of shopliftrOffer endDate \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_deals_are_available_until_upcoming_minimum_of_shopliftrOffer_endDate(String shopliftrCeltraUrl,
			String countryCode, String networkId) throws Throwable {
//        1. Date will be available based on the retailer time zone.
//        Ex- if in UTC end date is  "2021-05-30T04:59:00.000Z" then based on EST(UTC - 5.00) date will be 2021-05-29
//        2. If an offer has expired yesterday, show upcoming minimum of shopliftrOffer endDate, AND NOT yesterday's offer
//        3. Check offers on landing page is based on increasing order, and is not expired, and always even number of offers
//        on landing page.
		readPropertyFile = new ReadPropertyFile();
		long minimumOfShopliftrOfferEndDate = dataContainer.getMinimumOfShopliftrOfferEndDate();
		String monthDateFormat = TimeUtils.convertTimestampInMillisToMonthDateFormat(minimumOfShopliftrOfferEndDate,
				countryCode, networkId);

		String actualDealsValiditySpanText = retailerLandingPage.getDealsValiditySpanText();
		System.out.println("actualDealsValiditySpanText: " + actualDealsValiditySpanText);
		String expectedDealsValidityDate = readPropertyFile.getDealsValidityDate(monthDateFormat);
		Assert.assertEquals(actualDealsValiditySpanText, expectedDealsValidityDate);
	}

	@Then("^Verify Sale prices are valid with your retailer Card only \"([^\"]*)\"$")
	public void verify_Sale_prices_are_valid_with_your_retailer_Card_only(String retailerName) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String actualRetailerCardSpanText = retailerLandingPage.getRetailerCardSpanText();
		System.out.println("actualRetailerCardSpanText: " + actualRetailerCardSpanText);
		String expectedRetailerCardSpanText = readPropertyFile.getSalePricesWithRetailerCard(retailerName);
		Assert.assertEquals(expectedRetailerCardSpanText, actualRetailerCardSpanText);
	}

	@Then("^verify omniUser mongo collection \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_omniUser_mongo_collection(String shopliftrCeltraUrl, String countryCode, String networkId,
			String DID) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		String identifier = DID;
		String CID = "";
		if (expectedCeltraUrl.contains("unknown")) {
			identifier = DID;
		} else {
			CID = dataContainer.getCID();
		}
		String expectedLandingPageUrl = dataContainer.getLandingPageUrl();
		mongoDbUtils = new MongoDBUtils();
		String result = mongoDbUtils.verifyOmniUserMongoCollection(expectedCeltraUrl, expectedLandingPageUrl,
				countryCode, networkId, DID, CID);
		Assert.assertEquals("MATCHED", result);
	}

	@Given("^remove older omniUser mongo collection documents \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void remove_older_omniUser_mongo_collection_documents(String shopliftrCeltraUrl, String countryCode,
			String networkId, String DID, String retailerName) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		String identifier = DID;
		String CID = "";
		if (expectedCeltraUrl.contains("unknown")) {
			identifier = DID;
		} else {
			httpResponse = new HttpResponse();
			CID = httpResponse.getCidForDidUsingCardlinkPostCall(countryCode, networkId, DID);
			dataContainer.setCID(CID);
			identifier = CID;
		}
//		mongoDbUtils = new MongoDBUtils();
		String result = mongoDbUtils.removeOlderOmniUserMongoCollectionDocuments(expectedCeltraUrl, countryCode,
				networkId, DID, CID);
		Assert.assertEquals("DELETED", result);

	}

	@Then("^Verify on clicking shop online button retailer website page is seen \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_on_clicking_shop_online_button_retailer_website_page_is_seen(String countryCode,
			String networkId, String retailerName) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
//        retailerName  = readPropertyFile.getRetailerName(countryCode, networkId, retailerName);
		String expectedRetailerWebsitePage = readPropertyFile.getRetailerWebsitePage(countryCode, networkId,
				retailerName);

		Reporter.addStepLog("Navigating to " + retailerName + " website page: " + expectedRetailerWebsitePage);
		retailerLandingPage.clickShopOnlineButton();

		(new WebDriverWait(driver, 10)).until(ExpectedConditions.numberOfWindowsToBe(2));
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		String actualRetailerWebsitePage = driver.getCurrentUrl();
		sharedResource.addScreenshot(driver);
		driver.close();
		driver.switchTo().window(tabs.get(0));
		System.out.println("actualRetailerWebsitePage on browser is: " + actualRetailerWebsitePage);
		softAssert.assertEquals(expectedRetailerWebsitePage, actualRetailerWebsitePage);
	}

	@Then("^Verify on clicking save to wallet button wallet pass page is seen \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_on_clicking_save_to_wallet_button_wallet_pass_page_is_seen(String shopliftrCeltraUrl,
			String countryCode, String networkId, String DID, String retailerName) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		httpResponse = new HttpResponse();

		String identifier = DID;
		String CID = "";
		if (expectedCeltraUrl.contains("unknown")) {
			identifier = DID;
		} else {
			CID = httpResponse.getCidForDidUsingCardlinkPostCall(countryCode, networkId, DID);
		}

		String expectedLandingPageUrl = dataContainer.getLandingPageUrl();
		mongoDbUtils = new MongoDBUtils();
		String[] mongoFields = { "mfdPassworksCouponId", "mfdPassworksLandingURL" };

		LinkedHashMap<String, String> result = mongoDbUtils.getValueOfFieldsFromOmniUserMongoCollection(
				expectedCeltraUrl, expectedLandingPageUrl, countryCode, networkId, DID, CID, mongoFields);

		retailerLandingPage.clickSaveToWalletButton();
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.numberOfWindowsToBe(2));

		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		System.out.println("getWindowHandles size: " + tabs.size());
		driver.switchTo().window(tabs.get(1));

		String actualWalletPassPage = driver.getCurrentUrl();
		String expectedWalletPassPage = result.get("mfdPassworksLandingURL");
		System.out.println("actualWalletPassPage on browser is: " + actualWalletPassPage);
		System.out.println("expectedWalletPassPage from MongoDB collection: " + expectedWalletPassPage);
		Assert.assertEquals(expectedWalletPassPage, actualWalletPassPage);

		String QRCodeURL = walletPassPage.getQRCodeURL();
		boolean areElementsPresentOnWalletPassPage = walletPassPage
				.verifyPresenceAndContentOfElementsOnWalletPassPage();
		sharedResource.addScreenshot(driver);
		driver.close();
		driver.switchTo().window(tabs.get(0));
		Assert.assertEquals(expectedWalletPassPage + ".qrcode", QRCodeURL);
		Assert.assertTrue(areElementsPresentOnWalletPassPage);
	}

	@Given("^Set value of imageUrl field as empty for the first valid promotion \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void set_value_of_imageUrl_field_as_empty_for_the_first_valid_promotion(String shopliftrCeltraUrl,
			String countryCode, String networkId, String DID) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		dataContainer.setCeltraLink(expectedCeltraUrl);

		String identifier = DID;
		String CID = "";
		if (shopliftrCeltraUrl.contains("Unknown")) {
			identifier = DID;
		} else {
			httpResponse = new HttpResponse();
			CID = httpResponse.getCidForDidUsingCardlinkPostCall(countryCode, networkId, DID);
			dataContainer.setCID(CID);
			identifier = CID;
		}

		String[] fields = { "_id", "promotion", "imageUrl" };
		mongoDbUtils = new MongoDBUtils();
		LinkedHashMap<String, String> firstValidPromotionFieldDetails = mongoDbUtils
				.getFirstValidPromotionFieldDetailsForAnIdentiferFromShopliftrOfferMongoDBCollection(expectedCeltraUrl,
						networkId, identifier, fields);
		dataContainer.setDocumentID(firstValidPromotionFieldDetails.get("_id"));
		dataContainer.setPromotion(firstValidPromotionFieldDetails.get("promotion"));
		dataContainer.setImageUrl(firstValidPromotionFieldDetails.get("imageUrl"));

		LinkedHashMap<String, String> queryMap = new LinkedHashMap<String, String>();
		queryMap.put("_id", firstValidPromotionFieldDetails.get("_id"));
		LinkedHashMap<String, String> setDataMap = new LinkedHashMap<String, String>();
		setDataMap.put("imageUrl", "");

		mongoDbUtils.setValueOfFieldsInShopliftrOfferMongoDBCollection(queryMap, setDataMap);
	}

	@Given("^Extract value of fields from celtra call response and verify fallback offer creative info \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void extract_value_of_fields_from_celtra_call_response_and_verify_fallback_offer_creative_info(
			String shopliftrCeltraUrl, String countryCode, String networkId, String DID) throws Throwable {
		readPropertyFile = new ReadPropertyFile();
		httpResponse = new HttpResponse();
		System.out.println("DID: " + DID);
		String expectedCeltraUrl = readPropertyFile.getCeltraUrl(shopliftrCeltraUrl, countryCode, networkId, DID);
		LinkedHashMap<String, String> valueOfFieldsFromCeltraCallResponse = httpResponse
				.extractValueOfFieldsFromCeltraCallResponseAndVerifyFallbackOfferCreativeInfo(expectedCeltraUrl,
						countryCode, networkId);
		String expectedLandingPageUrl = valueOfFieldsFromCeltraCallResponse.get("landingPageUrl");

		dataContainer.setLandingPageUrl(expectedLandingPageUrl);
	}

	@Then("^Verify fallback offer creative on retailer landing page for the modified offer \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_fallback_offer_creative_on_retailer_landing_page_for_the_modified_offer(String countryCode,
			String networkId) throws Throwable {
		boolean isFallbackAdDisplayed = retailerLandingPage.verifyFirstOfferHasFallbackOfferCreativeOnLandingPage();
		sharedResource.addScreenshot(driver);
		Assert.assertTrue(isFallbackAdDisplayed);
	}

	@Then("^Reset original value of imageUrl field for the first valid promotion \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void reset_original_value_of_imageUrl_field_for_the_first_valid_promotion(String shopliftrCeltraUrl,
			String countryCode, String networkId, String DID) throws Throwable {
		mongoDbUtils = new MongoDBUtils();
		String documentID = dataContainer.getDocumentID();
		String promotion = dataContainer.getPromotion();
		String imageUrl = dataContainer.getImageUrl();

		LinkedHashMap<String, String> queryMap = new LinkedHashMap<String, String>();
		queryMap.put("_id", documentID);
		LinkedHashMap<String, String> setDataMap = new LinkedHashMap<String, String>();
		setDataMap.put("imageUrl", imageUrl);

		mongoDbUtils.setValueOfFieldsInShopliftrOfferMongoDBCollection(queryMap, setDataMap);
	}

	@Given("^Excel file is saved to a location in a proper format \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void validateExcel_File_Format(String location, String filename, String countryCode, String networkId,
			String flowType) {
		boolean formatMatched = retailerLandingPage.validatecsvFileFormat(location, filename, countryCode, networkId,
				flowType);
		Assert.assertTrue(formatMatched);
	}

	@Then("^Validate excel field values against shopLiftrUserOffer DB \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void validate_excel_field_values_against_shopLiftrUserOffer_DB(String filename, String networkId,
			String identifier, String flowType) throws Throwable {
//		String filecontent = ReadCSVFile.readFile(filename);
//		JSONObject content1 = utils.FileFormatterJSON.formatJSON(filecontent);
////        JSONObject content = utils.CSVtoJSON.covert();
////        System.out.println("Excel content : " + content);
//		mongoDbUtils = new MongoDBUtils();
//		S result = mongoDbUtils.validateidomoOffers(filecontent, networkId, identifier, flowType);
//		System.out.println("result is -------------------------" + result);
//		softAssert.assertEquals(result, filecontent);
//        Assert.assertEquals("Offers details on landing page " + result.get("isMatched")+ " with MongoDB","MATCHED", result.get("isMatched"));
//        boolean status= utils.validateidomoOffers(String filecontent, String networkId, String identifier, String flowType)

	}

//    @Given("^video url from csv file$")
	public static void main(String args[]) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
//        throw new PendingException();
//        utils.AppTest.appTest();

//        System.out.println("Before lauunching video");
//            driver.navigate().to("https://c.idomoo.com/4637/0001/4354778097224388c6231c1aec1d259594aeb22060d9da0316d5f18f3e564613.mp4");
//            System.out.println("Before isdisplayed");
//            driver.findElement(By.xpath("//video[@name='media']/src")).isDisplayed();
//            System.out.println("After isdisplayed");
////            Thread.sleep(100000);
//            driver.close();
//		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
//		Date date = new Date();
//		System.out.println("================");
//		Player audioPlayer = null;
		System.setProperty("webdriver.driver.chrome", "./chromedriver.exe");
		WebDriver driver = new ChromeDriver();
//
//		JavascriptExecutor js = (JavascriptExecutor) driver;
//		//calling the method
//		js.executeScript("document.getElement(By.xpath('//video')).play()");
//		Date dateStart = new Date();
//		System.out.println("1====" + format.format(dateStart));
//		driver.get("https://c.idomoo.com/4637/0001/4354778097224388c6231c1aec1d259594aeb22060d9da0316d5f18f3e564613.mp4");
//		driver.quit();
//		System.out.println("2====" + format.format(dateStart));


		/*
		 * ATUTestRecorder recorder;
		 *
		 *
		 *
		 * //create an object of ATUTestRecorder class and pass 3 parameters explained
		 * above. recorder = new ATUTestRecorder("C:/Recordings/Test/","Script_Video_" +
		 * format.format(date), false); System.out.println("================");
		 *
		 * //To start video recording. // StopWatch stopwatch = new StopWatch(); //
		 * stopwatch.start();

		 * recorder.start(); System.out.println("2===="+format.format(dateStart));
		 *
		 *
		 * // driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 * recorder.stop(); Date dateStop=new Date();
		 *
		 * // stopwatch.stop(); // long x = stopwatch.getTime();
		 *
		 * //Convert the result to a string // String numberAsString = Long.toString(x);
		 *
		 *
		 *
		 *
		 * // Get msec from each, and subtract.long diff = d2.getTime() - d1.getTime();
		 * // long diffSeconds = diff / 1000; // long diffMinutes = diff / (60 * 1000);
		 * // long diffHours = diff / (60 * 60 * 1000); System.out.println("DateStart "
		 * + dateStart + " seconds."); System.out.println("DateStop " + dateStop +
		 * " seconds.");
		 *
		 *
		 *
		 *
		 * // System.out.println("Total time taken to play video: " + x); //
		 * System.out.println("Total time taken to play video: " + numberAsString);
		 */
//		Eyes eyes = new Eyes();
//		String apiKey="R3PCM8VYt5PkSB3AC99100SWPN9YAN1093notlZj0r104hKh8A110";
//		eyes.setApiKey(apiKey);
//		String env=eyes.getEnvName();
//		String app= eyes.getAppName();
//		System.out.println("env name: " +env);
//		System.out.println("app name: " + app);
//		System.out.println("starting video");
//
//	eyes.open(driver,
//			"https://c.idomoo.com/4637/0001/4354778097224388c6231c1aec1d259594aeb22060d9da0316d5f18f3e564613.mp4",
//			"SQA",
//			new RectangleSize(800,600));
//	System.out.println("end of video");



//
//		    EmbeddedMediaPlayerComponent EMPC = new EmbeddedMediaPlayerComponent();
//		    EMPC.mediaPlayer().media().startPaused("https://c.idomoo.com/4637/0001/4354778097224388c6231c1aec1d259594aeb22060d9da0316d5f18f3e564613.mp4");
//		    EMPC.mediaPlayer().controls().play();
    }

}
