package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class CreateCampaign {

    WebDriver driver;
    public CreateCampaign(WebDriver driver) {
        this.driver = driver;
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        PageFactory.initElements(this.driver, this);

    }

    @FindBy(xpath = "//*[@id=\"services\"]/div[2]/a/i")
    WebElement creatCampaignIcon;

    @FindBy(xpath = "//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/ul/li/a")
    WebElement settingIconInCreateCampaign;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[1]/select-configured-network/select-dropdown/span/span[1]/span/span[2]")
    WebElement createCampaignDropdown;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[2]/input")
    WebElement campaignName;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[5]/div/div[2]/date-picker/div/input")
    WebElement adWeekStart;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[5]/div/div[3]/input")
    WebElement noOfAdWeek;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[6]/input")
    WebElement adBudget;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[7]/textarea")
    WebElement upc;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span/span[2]")
    WebElement flowTypeDropDown;

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[4]/select-campaign-type/select-dropdown/span/span[1]/span")
    WebElement currentCampaignType;

    @FindBy(xpath = "//li[text()='Known']")
    WebElement knownFlow;

    @FindBy(xpath = "//label[text()='Sponsored Ad Budget Amount (budget amount does not impact ad delivery)']")
    WebElement budgetField;

    public WebElement getBudgetField() {
        return budgetField;
    }

    public WebElement getFlowTypeDropDown() {
        return flowTypeDropDown;
    }

    public WebElement getCurrentCampaignType() {
        return currentCampaignType;
    }

    public WebElement getKnownFlow() {
        return knownFlow;
    }

    public WebElement getUnknownFlow() {
        return unknownFlow;
    }


    @FindBy(xpath = "//li[text()='Unknown']")
    WebElement unknownFlow;


    public WebElement getNoOfAdWeek() {
        return noOfAdWeek;
    }

    public WebElement getAdBudget() {
        return adBudget;
    }

    public WebElement getUpc() {
        return upc;
    }

    public WebElement getAdWeekStart() {

        return adWeekStart;
    }

    public WebElement getCampaignName() {
        return campaignName;
    }

    public WebElement getAdvertiserName() {
        return advertiserName;
    }

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[3]/input")
    WebElement advertiserName;
    public WebElement getCampagin_save() {
        return campagin_save;
    }

    @FindBy(xpath = "//*[@id=\"main-section\"]/router-view/div[1]/div/div/div[1]/div/button[2]")
    WebElement campagin_save;

    public WebElement getCreateCampaignDropdown() {
        return createCampaignDropdown;
    }

    public WebElement getCreateCampaignIcon() {
        return creatCampaignIcon;
    }

    public WebElement getSettingIconInCreateRetailer() {
        return settingIconInCreateCampaign;
    }

    @FindBy(xpath = "//*[@id=\"main-section\"]/router-view/div[2]/div/ul/li")
    WebElement errorMessageWithWrongDate;

    public WebElement getErrorMessageWithWrongDate() {
        return errorMessageWithWrongDate;
    }

    @FindBy(xpath = "//*[@id=\"uk-switch-place\"]/div/compose/div/div/div/div/div[5]/div/div[1]/ad-slot-priority/select-dropdown/span/span[1]/span/span[2]")
    WebElement adSlotPriorityDrowDown;

    @FindBy(xpath = "//li[text()=\"High\"]")
    WebElement adSlotPriorityHigh;

    public WebElement getAdSlotPriorityDrowDown() {
        return adSlotPriorityDrowDown;
    }

    public WebElement getAdSlotPriorityHigh() {
        return adSlotPriorityHigh;
    }

    public WebElement getAdSlotPriorityLow() {
        return adSlotPriorityLow;
    }

    @FindBy(xpath = "//li[text()=\"Low\"]")
    WebElement adSlotPriorityLow;


}
