package utils;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class MongoDBUtils {
    ReadPropertyFile readPropertyFile;
    String mongoDBConnectionString;


    public MongoDBUtils() {
        try {
            readPropertyFile = new ReadPropertyFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mongoDBConnectionString = readPropertyFile.getMongoDBConnectionString();
    }

    public String getCurrentDateTime(int minutesToSubtract) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime datetime = LocalDateTime.now(ZoneOffset.UTC);
//            System.out.println(datetime);
        datetime = datetime.minusMinutes(minutesToSubtract);
        String aftersubtraction = datetime.format(formatter);
//            System.out.println(aftersubtraction);
        String currentDate = aftersubtraction.substring(0, 10);
        String currentTime = aftersubtraction.substring(11, 19);
        String dateStr = currentDate + " " + currentTime + ".00 UTC";
        System.out.println(dateStr);
        return dateStr;
    }

    public String validateWakefernPromos() {
        String result = "{ \"data\" : [";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("vaultPromotion").withReadPreference(ReadPreference.primaryPreferred());

            // Fetching all the documents from the mongodb.

            FindIterable<Document> fi = collection.find();
            MongoCursor<Document> cursor = fi.iterator();
            int count = 0;
            JSONObject jsonObj = null;
            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    count++;
                    result = result + data + ",";
                    System.out.println("Check for the data" + cursor.next().toJson());
                }
                result = result.substring(0, result.length() - 1) + "]}";
                System.out.println("Result for promos is: " + result);
                if (count > 0) {
                    jsonObj = new JSONObject(result);
                    System.out.println("JSON object of promos is :" + jsonObj);
                    JSONArray jsonArray = jsonObj.getJSONArray("data");
                    Set<String> uniqueIDs = new HashSet<String>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = (JSONObject) jsonArray.get(i);
                        System.out.println("Data for obj.getString(\"promotionId\") " + obj.getString("promotionId"));
                        String ID = obj.getString("promotionId");
                        System.out.println("ID IS " + ID);
                        uniqueIDs.add(ID);
                    }
                    System.out.println("Promo ids set is : " + uniqueIDs);
                    if (uniqueIDs.size() != jsonArray.length()) {
                        result = "promotion details have duplicate values";
                    }
                } else {
                    result = "incorrect number of promo records";
                }
            } finally {
                cursor.close();
            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateUnknownUsersData() {
        String result = "{ \"data\" : [";
        // string uri connection
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("unknownUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            // Fetching all the documents from the mongodb.
            FindIterable<Document> fi = collection.find();
            MongoCursor<Document> cursor = fi.iterator();
            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    result = result + data + ",";
//                    System.out.println("Check for the data" + cursor.next().toJson());
                }
                result = result.substring(0, result.length() - 1) + "]}";
                System.out.println("Result for wakefern unknown user offers is: " + result);
            } finally {
                cursor.close();
            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateJobStatus(String jobName, String networkId) {
        String result = "";
        String jobStatus = "";
        String jobExitStatusCode = "";
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection("shopliftr-batch").withReadPreference(ReadPreference.primaryPreferred());

            String dateStr = getCurrentDateTime(5);
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(dateStr);
            System.out.println(date);

            BasicDBObject query = new BasicDBObject();
            query.put("jobInstance.jobName", jobName);
            query.put("jobParameters.countryCode.parameter", "USA");
            query.put("jobParameters.networkId.parameter", networkId);
            query.put("startTime", new BasicDBObject("$gte", date));
            System.out.println(query);

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data " + data);
                    JSONObject json = new JSONObject(data);
                    jobStatus = json.getString("status");
                    JSONObject jobExitStatus = json.getJSONObject("exitStatus");
//                    JSONObject obj = new JSONObject(jobExitStatus);
                    jobExitStatusCode = jobExitStatus.getString("exitCode");
                    System.out.println("Status of Job Run is " + jobStatus);
                    System.out.println("Exit status of Job Run is " + jobExitStatusCode);
                }

                if (jobStatus.equals("COMPLETED") && jobExitStatusCode.equals("COMPLETED")) {
                    result = "COMPLETED";
                } else {
                    result = "NOT COMPLETED";
                }

            } finally {
                cursor.close();
            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateCeltraOffers(String celtraResponse, String networkId, String cid) {
        String result = "";
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());

            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("cid", cid);
            System.out.println("Celtra Response is : " + celtraResponse);
//            System.out.println(query);

            JSONObject celtraJson = new JSONObject(celtraResponse);
            JSONArray offers = new JSONArray();

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data : " + data);
                    JSONObject jsonData = new JSONObject(data);
                    offers = jsonData.getJSONArray("offers");
                    System.out.println("Offers Array is : " + offers);
                }

                int count = 0;

                for (int i = 1; i <= 5; i++) {
                    String name = "name_" + i;
                    String offerId = "offer_id_" + i;
                    String brand = "brand_" + i;
                    String shopliftrAdId = "shopliftr_ad_id_" + i;
                    String manufacturer = "manufacturer_" + i;
                    String customPrice = "custom_price_" + i;
                    String size = "size_" + i;
                    String saleQty = "sale_qty_" + i;
                    String type = "type_" + i;
                    String imageUrl = "image_url_" + i;

                    for (int j = 0; j < offers.length(); j++) {
                        JSONObject offersJson = (JSONObject) offers.get(j);
                        if ((offersJson.get("name").equals(celtraJson.get(name)))
                                && (offersJson.get("offerId").equals(celtraJson.get(offerId)))
                                && (offersJson.get("brand").equals(celtraJson.get(brand)))
                                && (offersJson.get("shopliftrAdId").equals(celtraJson.get(shopliftrAdId)))
                                && (offersJson.get("manufacturer").equals(celtraJson.get(manufacturer)))
                                && (offersJson.get("customPrice").equals(celtraJson.get(customPrice)))
                                && (offersJson.get("size").equals(celtraJson.get(size)))
                                && (offersJson.get("saleQty").equals(celtraJson.get(saleQty)))
                                && (offersJson.get("type").equals(celtraJson.get(type)))
                                && (offersJson.get("imageUrl").equals(celtraJson.get(imageUrl)))) {
                            count++;
                        }
                    }
                }

                System.out.println(count);

                if (count == 5) {
                    result = "MATCHED";
                } else {
                    result = "NOT MATCHED";
                }

            } finally {
                cursor.close();
            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public LinkedHashMap<String,String> compareOffersDetailsOnLandingPageWithShopliftrOfferMongoDBCollection(HashMap<String, String> offersDetailsLinkedHashmapFromLandingPage, String shopliftrCeltraUrl, String networkId, String identifier) {

//         We are getting below 4 fields for each offer from LinkedHashMap:
//        "customPrice" in shopliftrOffer mongoDB collection represents "adDiscountText" on landing page, e.g.: "2 for $9",
//        "brand" in shopliftrOffer mongoDB collection represents "adVerbiage1" on landing page, e.g.: "Larabar",
//        "name" in shopliftrOffer mongoDB collection represents "adVerbiage2" on landing page, e.g.: "Fruit & Nut Bar",
//        "additional" shopliftrOffer in mongoDB collection represents "adAdQualifier" on landing page, e.g.: "Selected Varieties Only",

        LinkedHashMap<String,String> result = new LinkedHashMap<String,String>();

        String isMatched = "";
        int offersCountFromLandingPage = offersDetailsLinkedHashmapFromLandingPage.size() / 4;
        int offersCountInDb = 0;
        long minimumOfShopliftrOfferEndDate = 0;
        JSONObject offerDetails=getOffersFromDB(shopliftrCeltraUrl,identifier,networkId);
        JSONArray offers=offerDetails.getJSONArray("offers");
//        JSONArray offersJsonArrayInShopliftrUserOfferMongoDBCollection = getOffersJsonArrayInShopliftrUserOfferMongoDBCollection(shopliftrCeltraUrl, networkId, identifier);

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "shopliftrOffer";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

//            for (int i = 1; i <= offersJsonArrayInShopliftrUserOfferMongoDBCollection.length(); i++) {
//                JSONObject objects = offersJsonArrayInShopliftrUserOfferMongoDBCollection.getJSONObject(i-1);
//
//                BasicDBObject query = new BasicDBObject();
//                String promotion = (String) objects.get("promotion");
//                query.put("promotion", promotion);
//
//                System.out.println(query);
//
//                FindIterable<Document> fi = collection.find(query);
//                MongoCursor<Document> cursor = fi.iterator();
//
//                String customPrice = "adDiscountText_" + i;
//                String brand = "adVerbiage1_" + i;
//                String name = "adVerbiage2_" + i;
//                String additional = "adAdQualifier_" + i;
//
//                System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(customPrice): " + offersDetailsLinkedHashmapFromLandingPage.get(customPrice));
//                System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(brand): " + offersDetailsLinkedHashmapFromLandingPage.get(brand));
//                System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(name): " + offersDetailsLinkedHashmapFromLandingPage.get(name));
//                System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(additional): " + offersDetailsLinkedHashmapFromLandingPage.get(additional));
//                JSONObject offersJson = null;
//                long created;
//                long latestCreated = 0L;
//                long endDate;
//                long currentTimeMillis;
//                try {
//                    while (cursor.hasNext()) {
//                        String data = cursor.next().toJson();
//                        System.out.println("Data : " + data);
//                        offersJson = new JSONObject(data);
//
////                        Extract offer created's from JSONObject offersJson in form of timestamp in millis
//                        created = Long.parseLong(offersJson.get("created").toString().replaceAll("\\D", ""));
//
////                        If created is less than latestCreated
//                        if (latestCreated != 0 && created <= latestCreated) {
//                            continue;
//                        }
//                        else {
//                            latestCreated = created;
//                        }
//
//
////                        Extract offer endDate's from JSONObject offersJson in form of timestamp in millis
//                        endDate = Long.parseLong(offersJson.get("endDate").toString().replaceAll("\\D", ""));
//
//                        currentTimeMillis = TimeUtils.getCurrentTimeMillis();
//                        System.out.println("created: " + created);
//                        System.out.println("Offer endDate: " + endDate);
//                        System.out.println("currentTimeMillis: " + currentTimeMillis);
//
////                        If offer has expired in DB (i.e. offer's endDate is in past), don't consider them for comparison on landing page.
//                        if (endDate < currentTimeMillis) {
//                            cursor.close();
//                            continue;
//                        }
//                        if (minimumOfShopliftrOfferEndDate == 0 || endDate < minimumOfShopliftrOfferEndDate) {
//                            minimumOfShopliftrOfferEndDate = endDate;
//                        }
//
////                        if ((offersJson.get("customPrice").equals(offersDetailsLinkedHashmapFromLandingPage.get(customPrice)))
////                                && (offersJson.get("brand").equals(offersDetailsLinkedHashmapFromLandingPage.get(brand)))
////                                && (offersJson.get("name").equals(offersDetailsLinkedHashmapFromLandingPage.get(name)))
////                                && (offersJson.get("additional").equals(offersDetailsLinkedHashmapFromLandingPage.get(additional)))) {
////                            offersCountInDb++;
////                            System.out.println("Offer Count: " + offersCountInDb);
////                            System.out.println();
////                        }
//                    }
//
//                    if ((offersJson.get("customPrice").equals(offersDetailsLinkedHashmapFromLandingPage.get(customPrice)))
//                            && (offersJson.get("brand").equals(offersDetailsLinkedHashmapFromLandingPage.get(brand)))
//                            && (offersJson.get("name").equals(offersDetailsLinkedHashmapFromLandingPage.get(name)))
//                            && (offersJson.get("additional").equals(offersDetailsLinkedHashmapFromLandingPage.get(additional))))
//                    {
//                        offersCountInDb++;
//                        System.out.println("Offer Count: " + offersCountInDb);
//                        System.out.println();
//                    }
//                } catch (Exception e) {
//                    System.out.println(e.getCause());
//                } finally {
//                    cursor.close();
//                }
//            }
//            mongoClient.close();
//            System.out.println("Total offersCountInDb:" + offersCountInDb);
//
//            if (offersCountInDb == offersCountFromLandingPage) {
//                isMatched = "MATCHED";
//            } else {
//                isMatched = "NOT MATCHED";
//            }
//            result.put("isMatched" , isMatched);
//            result.put("offersCountInDb" , String.valueOf(offersCountInDb));
//            result.put("minimumOfShopliftrOfferEndDate", String.valueOf(minimumOfShopliftrOfferEndDate));
//        } catch (Exception e) {
//            System.out.println("Some error in compareOffersDetailsOnLandingPageWithShopliftrOfferMongoDBCollection() method.");
//            System.out.println(e.getCause());
//        } finally {
//            System.out.println("Offers Details On Landing Page " + isMatched + " With ShopliftrUserOffer MongoDB Collection");
//            return result;
//        }
//    }

                    for (int i = 0; i < offers.length(); i++) {
//validating sponsored ad
                        JSONObject offerJson = (JSONObject) offers.get(i);
                        BasicDBObject query2 = new BasicDBObject();
//System.out.println("Processed Date is "+new Date(Long.parseLong(jsonData.getString("processed"))));
                        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ");
                        formater.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ZZZ").parse(formater.format(new Date((Long) offerDetails.getJSONObject("processed").get("$date"))));


                        query2.put("processed", date);
                        query2.put("networkId", offerDetails.getString("networkId"));
                        query2.put("offerId", offerJson.getString("offerId"));
                        System.out.println("Query 2 is" + query2);
                        FindIterable<Document> offerIterable = collection.find(query2);
                        MongoCursor<Document> cursor = offerIterable.iterator();
                        String customPrice = "adDiscountText_" + (i + 1);
                        String brand = "adVerbiage1_" + (i + 1);
                        String name = "adVerbiage2_" + (i + 1);
                        String additional = "adAdQualifier_" + (i + 1);


                        System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(customPrice): " + offersDetailsLinkedHashmapFromLandingPage.get(customPrice));
                        System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(brand): " + offersDetailsLinkedHashmapFromLandingPage.get(brand));
                        System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(name): " + offersDetailsLinkedHashmapFromLandingPage.get(name));
                        System.out.println("offersDetailsLinkedHashmapFromLandingPage.get(additional): " + offersDetailsLinkedHashmapFromLandingPage.get(additional));
                        JSONObject offersJson = null;
                        long created;
                        long latestCreated = 0L;
                        long endDate;
                        long currentTimeMillis;
                        try {
                            while (cursor.hasNext()) {
                                String data = cursor.next().toJson();
                                System.out.println("Data : " + data);
                                offersJson = new JSONObject(data);


// Extract offer created's from JSONObject offersJson in form of timestamp in millis
                                created = Long.parseLong(offersJson.get("created").toString().replaceAll("\\D", ""));


// If created is less than latestCreated
                                if (latestCreated != 0 && created <= latestCreated) {
                                    continue;
                                } else {
                                    latestCreated = created;
                                }


// Extract offer endDate's from JSONObject offersJson in form of timestamp in millis
                                endDate = Long.parseLong(offersJson.get("endDate").toString().replaceAll("\\D", ""));


                                currentTimeMillis = TimeUtils.getCurrentTimeMillis();
                                System.out.println("created: " + created);
                                System.out.println("Offer endDate: " + endDate);
                                System.out.println("currentTimeMillis: " + currentTimeMillis);
// If offer has expired in DB (i.e. offer's endDate is in past), don't consider them for comparison on landing page.
                                if (endDate < currentTimeMillis) {
                                    cursor.close();
                                    continue;
                                }
                                if (minimumOfShopliftrOfferEndDate == 0 || endDate < minimumOfShopliftrOfferEndDate) {
                                    minimumOfShopliftrOfferEndDate = endDate;
                                }

                            }

// PART OF IF CONDITION
// && (offersJson.get("brand").equals(offersDetailsLinkedHashmapFromLandingPage.get(brand)))
// && (offersJson.get("name").equals(offersDetailsLinkedHashmapFromLandingPage.get(name)))
// && (offersJson.get("additional").equals(offersDetailsLinkedHashmapFromLandingPage.get(additional)))
                            if ((offersJson.get("customPrice").equals(offersDetailsLinkedHashmapFromLandingPage.get(customPrice)))) {
                                System.out.println("-->" + offersJson.get("brand") + "<-->" + offersDetailsLinkedHashmapFromLandingPage.get(brand));
                                System.out.println("-->" + offersJson.get("name") + "<-->" + offersDetailsLinkedHashmapFromLandingPage.get(name));
                                System.out.println("-->" + offersJson.get("additional") + "<-->" + offersDetailsLinkedHashmapFromLandingPage.get(additional));
                                offersCountInDb++;
                                System.out.println("Offer Count: " + offersCountInDb);
                                System.out.println();
                            }
                        } catch (Exception e) {
                            System.out.println(e.getCause());
                        } finally {
                            cursor.close();
                        }
                    }
                    mongoClient.close();
                    System.out.println("Total offersCountInDb:" + offersCountInDb);


                    if (offersCountInDb == offersCountFromLandingPage) {
                        isMatched = "MATCHED";
                    } else {
                        isMatched = "NOT MATCHED";
                    }
                    result.put("isMatched", isMatched);
                    result.put("offersCountInDb", String.valueOf(offersCountInDb));
                    result.put("minimumOfShopliftrOfferEndDate", String.valueOf(minimumOfShopliftrOfferEndDate));
                } catch (Exception e) {
                    System.out.println("Some error in compareOffersDetailsOnLandingPageWithShopliftrOfferMongoDBCollection() method.");
                    System.out.println(e.getCause());
                } finally {
                    System.out.println("Offers Details On Landing Page " + isMatched + " With ShopliftrUserOffer MongoDB Collection");
                    return result;
                }


            }


    public JSONObject getOffersFromDB(String celtraResponse, String identifier, String networkId) {

        String flowType = celtraResponse.toLowerCase().contains("unknown") ? "UNKNOWN" : "KNOWN";
        String result = "";
        MongoClient mongoClient = null;
        JSONObject jsonData = null;
        try {

            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            mongoClient = new MongoClient(uri);
            MongoIterable<String> names = mongoClient.listDatabaseNames();
            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
//            MongoCollection<Document> collection1 = db.getCollection("shopliftrOffer").withReadPreference(ReadPreference.primaryPreferred());
            // query parameters
            BasicDBObject query = new BasicDBObject();
            query.put("flowType", flowType);
            query.put("networkId", networkId);
            if (flowType == "KNOWN") {
                query.put("identifier", identifier);
            } else {
                query.put("identifier", identifier.toUpperCase());
            }
            System.out.println("Shoplifter user offer " + query);


            // mongo db find query
            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            while (cursor.hasNext()) {
                String data = cursor.next().toJson();
                jsonData = new JSONObject(data);
//                offers = jsonData.getJSONArray("offers");
//                    System.out.println("Offers Array is : " + offers);
            }

        } catch (Exception e) {
            e.printStackTrace();
            mongoClient.close();
        } finally {
            System.out.println("Inside Finally Block");
            mongoClient.close();
        }
        return jsonData;
    }

    public int getOffersCountInShopliftrUserOfferMongoDBCollection(String shopliftrCeltraUrl, String networkId, String CID) {
        int offersCountInDb = 0;
        JSONArray offersInDb;

        // Decide flowType. Either Shopliftr KNOWN or UNKNOWN flow
        String flowType = shopliftrCeltraUrl.contains("unknown") ? "UNKNOWN" : "KNOWN";

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "shopliftrUserOffer";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("identifier", CID);
            query.put("flowType", flowType);
            System.out.println(query);

            offersInDb = new JSONArray();

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data : " + data);
                    JSONObject jsonData = new JSONObject(data);
                    offersInDb = jsonData.getJSONArray("offers");
                    System.out.println("Offers Array from DB is : " + offersInDb);
                    offersCountInDb = offersInDb.length();
                    System.out.println("offersCountInDb: " + offersCountInDb);
                }
            } catch (Exception e) {
                System.out.println(e.getCause());
            } finally {
                cursor.close();
                mongoClient.close();
            }
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            return offersCountInDb;
        }
    }

    public JSONArray getOffersJsonArrayInShopliftrUserOfferMongoDBCollection(String shopliftrCeltraUrl, String networkId, String identifier) {
        int offersCountInDb = 0;
        JSONArray offersInDb = null;

        // Decide flowType. Either Shopliftr KNOWN or UNKNOWN flow
        String flowType = shopliftrCeltraUrl.toLowerCase().contains("unknown") ? "UNKNOWN" : "KNOWN";

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "shopliftrUserOffer";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            BasicDBObject query = new BasicDBObject();
            query.put("networkId", networkId);
            query.put("identifier", identifier);
            query.put("flowType", flowType);
            System.out.println(query);

            offersInDb = new JSONArray();

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data : " + data);
                    JSONObject jsonData = new JSONObject(data);
                    offersInDb = jsonData.getJSONArray("offers");
                    System.out.println("Offers Array from DB is : " + offersInDb);
                    offersCountInDb = offersInDb.length();
                    System.out.println("offersCountInDb: " + offersCountInDb);
                }
            } catch (Exception e) {
                System.out.println(e.getCause());
            } finally {
                cursor.close();
                mongoClient.close();
            }
        } catch (Exception e) {
            System.out.println(e.getCause());
        } finally {
            // Send sorted JSONArray based on offer's rank
            JSONArray sortedJsonArray = new JSONArray();

            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
            for (int i = 0; i < offersInDb.length(); i++) {
                jsonValues.add(offersInDb.getJSONObject(i));
            }
            Collections.sort( jsonValues, new Comparator<JSONObject>() {
                // Sort JSONArray based on offer's rank
                private static final String KEY_NAME = "rank";

                @Override
                public int compare(JSONObject a, JSONObject b) {
                    int valA = new Integer(0);
                    int valB = new Integer(0);

                    try {
                        valA = (int) a.get(KEY_NAME);
                        valB = (int) b.get(KEY_NAME);
                    }
                    catch (JSONException e) {
                        System.out.println("Error while sorting offers based on " + KEY_NAME);
                        System.out.println(e);
                    }

                    if(valA == valB)
                        return 0;
                    else if(valA > valB )
                        return 1;
                    else
                        return -1;
                }
            });

            for (int i = 0; i < offersInDb.length(); i++) {
                sortedJsonArray.put(jsonValues.get(i));
            }

            System.out.println("Sorted Offers Array from DB is : " + sortedJsonArray);

            return sortedJsonArray;
        }
    }

    public String verifyOmniUserMongoCollection(String celtraUrl, String landingPageUrl, String countryCode, String networkId, String DID, String CID) {
        LinkedHashMap<String,String> result = new LinkedHashMap<String,String>();

        String isMatched = "NOT MATCHED";
        int offersCountInDb = 0;

        // Decide dataFeedType. Either SHOPLIFTR or VAULT
        String dataFeedType = celtraUrl.contains("shopliftr") ? "SHOPLIFTR" : "VAULT";
        // Decide flowType. Either Shopliftr KNOWN or UNKNOWN flow
        String flowType = celtraUrl.contains("unknown") ? "UNKNOWN" : "KNOWN";

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "omniUser";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");
            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            BasicDBObject query = new BasicDBObject();
            query.put("countryCode", countryCode);
            query.put("digitalId", DID.toUpperCase()); System.out.println("999999999999999999999999999999999999999999999999999");

            if (networkId.contains(".")) {
                String[] arr = networkId.split("\\.");
                networkId = arr[0];
                String subnetwork = arr[1];
                System.out.println("networkId: " + networkId);
                System.out.println("subnetwork: " + subnetwork);
                query.put("networkId", networkId);
                query.put("subnetwork", subnetwork);
            }
            else {
                query.put("networkId", networkId);
            }
            query.put("dataFeedType", dataFeedType);
            query.put("flowType", flowType);
            System.out.println(query);

            JSONObject jsonData = null;

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data : " + data);
                    jsonData = new JSONObject(data);
                    System.out.println("jsonData: " + jsonData);
                }
            } catch (Exception e) {
                System.out.println(e.getCause());
            } finally {
                cursor.close();
                mongoClient.close();
            }

//            "_id" : ObjectId("5fca2324b0dea91c6074b38b"),
//    "countryCode" : "USA",
//    "digitalId" : "b4aed3e1-df31-477b-b9a0-942ad4746f80",
//    "fscId" : "100000012",
//    "networkId" : "4",
//    "mfdPassworksCouponId" : "8939876c-6a91-44f6-aca1-25fb6e9b9a7d",
//    "mfdPassworksLandingURL" : "https://pass.yt/c/TjHyGmdYrg",
//    "dataFeedType" : "SHOPLIFTR",
//    "flowType" : "UNKNOWN",
//    "created" : ISODate("2020-12-04T11:53:08.196Z"),
//    "updated" : ISODate("2021-06-25T10:22:36.733Z"),

            if ((jsonData.get("countryCode").equals(countryCode))
                    && (jsonData.get("digitalId").equals(DID))
                    && (jsonData.get("networkId").equals(networkId))
                    && (jsonData.get("dataFeedType").equals(dataFeedType))
                    && (jsonData.get("flowType").equals(flowType))) {
//                Extract offer updatedTime from JSONObject jsonData in form of timestamp in millis
                long updatedTime = Long.parseLong(jsonData.get("updated").toString().replaceAll("\\D", ""));
                long currentTimeMillis = TimeUtils.getCurrentTimeMillis();
                System.out.println("updatedTime: " + updatedTime);
                System.out.println("currentTimeMillis: " + currentTimeMillis);

//                Check if updatedTime was updated within last 10 minutes
                if (updatedTime > (currentTimeMillis - (10 * 60 * 1000))) {
//                    Check landing page ID on landing page matches with MongoDB record
                    String[] arrUrl = landingPageUrl.split("/");
                    String landingPageIdFromURL = arrUrl[arrUrl.length - 1];

                    String[] arrMongo = jsonData.get("_id").toString().split(":");
                    String landingPageIdFromMongoDB = arrMongo[arrMongo.length - 1].replaceAll("\\W", "");
                    System.out.println("landingPageId from Url: " + landingPageIdFromURL);
                    System.out.println("landingPageId from MongoDB: " +landingPageIdFromMongoDB);
                    if (landingPageIdFromURL.equals(landingPageIdFromMongoDB)) {
//                        Check for fscId when flowType is KNOWN
                        String[] arrCID = CID.split("-");
                        String fscId = arrCID[arrCID.length - 1];
                        if (flowType.equals("KNOWN") && jsonData.get("fscId").equals(fscId)) {
                            isMatched = "MATCHED";
                        }
                        else if (flowType.equals("UNKNOWN")) {
                            isMatched = "MATCHED";
                        }
                        else {
                            System.out.println("Flow type is KNOWN, but fscId does NOT match.");
                            isMatched = "NOT MATCHED";
                        }
                    }
                }
                else {
                    System.out.println("'updatedTime' was NOT updated within last 10 minutes in OmniUser MongoDB collection");
                }
            }

            System.out.println("OmniUser entry: " + isMatched);
            System.out.println();

            result.put("isMatched" , isMatched);
        } catch (Exception e) {
            System.out.println("Some error in verifyOmniUserMongoCollection() method.");
            System.out.println(e.getCause());
        } finally {
            System.out.println("omniUser entry: " + isMatched);
            return isMatched;
        }
    }

    public String removeOlderOmniUserMongoCollectionDocuments(String celtraUrl, String countryCode, String networkId, String DID, String CID) {
        String isDeleted = "DELETED";
        // Decide dataFeedType. Either SHOPLIFTR or VAULT
        String dataFeedType = celtraUrl.contains("shopliftr") ? "SHOPLIFTR" : "VAULT";
        // Decide flowType. Either Shopliftr KNOWN or UNKNOWN flow
        String flowType = celtraUrl.contains("unknown") ? "UNKNOWN" : "KNOWN";
        BasicDBObject query=null;

        try {
            //MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClientURI uri = new MongoClientURI("mongodb://mta:y38ngBDNHWmFgLE@sqa-1-shard-00-00-tbdu2.azure.mongodb.net:27017,sqa-1-shard-00-01-tbdu2.azure.mongodb.net:27017,sqa-1-shard-00-02-tbdu2.azure.mongodb.net:27017/admin.omnimfd&authMechanism=SCRAM-SHA-1&replicaSet=atlas-sqa&readPreference=primary&connectTimeoutms=6000&socketTimeoutMS=300000");
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "omniUser";

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection("omniUser").withReadPreference(ReadPreference.primaryPreferred());

             query = new BasicDBObject();
            query.put("countryCode", countryCode);
            query.put("digitalId", DID.toUpperCase());

            if (networkId.contains(".")) {
                String[] arr = networkId.split("\\.");
                networkId = arr[0];
                String subnetwork = arr[1];
                System.out.println("networkId: " + networkId);
                System.out.println("subnetwork: " + subnetwork);
                query.put("networkId", networkId);
                query.put("subnetwork", subnetwork);
            }
            else {
                query.put("networkId", networkId);
            }
            query.put("dataFeedType", dataFeedType);
            query.put("flowType", flowType);
            System.out.println(query);

            collection.deleteMany(query);
        } catch (Exception e) {
            System.out.println("Some error in removeOlderOmniUserMongoCollectionDocuments() method.");
            System.out.println(e.getCause());
            isDeleted = "NOT DELETED";
        }
        finally {
            System.out.println("omniUser entry: " + isDeleted);
            return isDeleted;
        }
    }

    public LinkedHashMap<String,String> getValueOfFieldsFromOmniUserMongoCollection(String celtraUrl, String landingPageUrl, String countryCode, String networkId, String DID, String CID, String[] fields) {
        LinkedHashMap<String,String> result = new LinkedHashMap<String,String>();

        // Decide dataFeedType. Either SHOPLIFTR or VAULT
        String dataFeedType = celtraUrl.contains("shopliftr") ? "SHOPLIFTR" : "VAULT";
        // Decide flowType. Either Shopliftr KNOWN or UNKNOWN flow
        String flowType = celtraUrl.contains("unknown") ? "UNKNOWN" : "KNOWN";

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "omniUser";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            BasicDBObject query = new BasicDBObject();
            query.put("countryCode", countryCode);
            query.put("digitalId", DID.toUpperCase());
            if (networkId.contains(".")) {
                String[] arr = networkId.split("\\.");
                networkId = arr[0];
                String subnetwork = arr[1];
                System.out.println("networkId: " + networkId);
                System.out.println("subnetwork: " + subnetwork);
                query.put("networkId", networkId);
                query.put("subnetwork", subnetwork);
            }
            else {
                query.put("networkId", networkId);
            }
            query.put("dataFeedType", dataFeedType);
            query.put("flowType", flowType);
            System.out.println(query);

            JSONObject jsonData = null;

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();
            try {
                while (cursor.hasNext()) {
                    String data = cursor.next().toJson();
                    System.out.println("Data : " + data);
                    jsonData = new JSONObject(data);
                    System.out.println("jsonData: " + jsonData);
                }
            } catch (Exception e) {
                System.out.println(e.getCause());
            } finally {
                cursor.close();
                mongoClient.close();
            }

//            "_id" : ObjectId("5fca2324b0dea91c6074b38b"),
//    "countryCode" : "USA",
//    "digitalId" : "b4aed3e1-df31-477b-b9a0-942ad4746f80",
//    "fscId" : "100000012",
//    "networkId" : "4",
//    "mfdPassworksCouponId" : "8939876c-6a91-44f6-aca1-25fb6e9b9a7d",
//    "mfdPassworksLandingURL" : "https://pass.yt/c/TjHyGmdYrg",
//    "dataFeedType" : "SHOPLIFTR",
//    "flowType" : "UNKNOWN",
//    "created" : ISODate("2020-12-04T11:53:08.196Z"),
//    "updated" : ISODate("2021-06-25T10:22:36.733Z"),

            for (String field: fields) {
                System.out.println("-----------------------HELLO1--------------------------");
                result.put(field, jsonData.getString(field));
                System.out.println("-----------------------HELLO2--------------------------");
            }
        } catch (Exception e) {
            System.out.println("Some error in getValueOfFieldsFromOmniUserMongoCollection() method.");
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public void setValueOfFieldsInShopliftrOfferMongoDBCollection(LinkedHashMap<String, String> queryMap, LinkedHashMap<String, String> setDataMap) {
        LinkedHashMap<String,String> result = new LinkedHashMap<String,String>();

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "shopliftrOffer";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            Document query = new Document();
            for(Map.Entry item : queryMap.entrySet()){
                if (item.getKey().toString().equals("_id")) {
                    query.append(item.getKey().toString(), new ObjectId(item.getValue().toString()));
                    System.out.println("Looking into _id");
                }
                else {
                    query.append(item.getKey().toString(), item.getValue().toString());
                }
                System.out.println("Query: " + item.getKey()+" "+item.getValue());
                System.out.println("Query: " + item.getKey()+" "+ new ObjectId(item.getValue().toString()));
            }
            Document setData = new Document();
            for(Map.Entry item : setDataMap.entrySet()){
                setData.append(item.getKey().toString(), item.getValue().toString());
                System.out.println("Set Data: " + item.getKey()+" "+item.getValue());
            }
            Document update = new Document();
            update.append("$set", setData);
            //To update single Document
            collection.updateOne(query, update);
            mongoClient.close();
        } catch (Exception e) {
            System.out.println("Some error in setValueOfFieldsInShopliftrOfferMongoDBCollection() method.");
            System.out.println(e.getCause());
        }
    }

    public LinkedHashMap<String,String> getFirstValidPromotionFieldDetailsForAnIdentiferFromShopliftrOfferMongoDBCollection(String shopliftrCeltraUrl, String networkId, String identifier, String [] fields) {
        LinkedHashMap<String,String> result = new LinkedHashMap<String,String>();

        int count = 0;
        JSONArray offersJsonArrayInShopliftrUserOfferMongoDBCollection = getOffersJsonArrayInShopliftrUserOfferMongoDBCollection(shopliftrCeltraUrl, networkId, identifier);

        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

//            MongoIterable<String> names = mongoClient.listDatabaseNames();

            String databaseName = "omnimfd";
            String collectionName = "shopliftrOffer";

            MongoDatabase db = mongoClient.getDatabase(databaseName);
            System.out.println("Database is connected");

            MongoCollection<Document> collection = db.getCollection(collectionName).withReadPreference(ReadPreference.primaryPreferred());

            long created;
            long latestCreated = 0L;

            outer_loop:
            for (int i = 1; i <= offersJsonArrayInShopliftrUserOfferMongoDBCollection.length(); i++) {
                JSONObject objects = offersJsonArrayInShopliftrUserOfferMongoDBCollection.getJSONObject(i-1);

                BasicDBObject query = new BasicDBObject();
                String promotion = (String) objects.get("promotion");
                query.put("promotion", promotion);

                System.out.println(query);

                FindIterable<Document> fi = collection.find(query);

                MongoCursor<Document> cursor = fi.iterator();



                try {
                    while (cursor.hasNext()) {
                        String data = cursor.next().toJson();
                        System.out.println("Data : " + data);
                        JSONObject offersJson = new JSONObject(data);

//                        Extract offer endDate's from JSONObject offersJson in form of timestamp in millis
                        long endDate = Long.parseLong(offersJson.get("endDate").toString().replaceAll("\\D", ""));
                        created = Long.parseLong(offersJson.get("created").toString().replaceAll("\\D", ""));
                        long currentTimeMillis = TimeUtils.getCurrentTimeMillis();
                        System.out.println("Offer endDate: " + endDate);
                        System.out.println("currentTimeMillis: " + currentTimeMillis);





//      *******************************************************************************


//                        If created is less than latestCreated
                        if (latestCreated != 0 && created <= latestCreated) {
                            continue;
                        }
                        else if (latestCreated != 0 && created >= currentTimeMillis) {
                            continue;
                        }
                        else {
                            latestCreated = created;
                        }
//            *****************************************************************************************







//                        If offer has expired in DB (i.e. offer's endDate is in past), ignore it.
                        if (endDate < currentTimeMillis) {
//                            cursor.close();
                            continue;
                        }

                        count++;

                        for (String field: fields) {
                            result.put(field, offersJson.get(field).toString());
                        }
                        String[] arrMongo = offersJson.get("_id").toString().split(":");
                        String documentID = arrMongo[arrMongo.length - 1].replaceAll("\\W", "");
                        result.put("_id", documentID);

//                        if (count == 1) {
//                            for (String field: fields) {
//                                result.put(field, offersJson.get(field).toString());
//                            }
//                            cursor.close();
//                            break outer_loop;
//                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getCause());
                } finally {
                    cursor.close();
                }

                if (result.size() == fields.length) {
                    break outer_loop;
                }

            }
            mongoClient.close();
        } catch (Exception e) {
            System.out.println("Some error in getFirstValidPromotionFieldDetailsForAnIdentiferFromShopliftrOfferMongoDBCollection() method.");
            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }

    public String validateidomoOffers(List<String> filecontent, String networkId, String flowType) {
        String result = "";
        try {
            MongoClientURI uri = new MongoClientURI(mongoDBConnectionString);
            MongoClient mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");

            MongoIterable<String> names = mongoClient.listDatabaseNames();

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
            MongoCollection<Document> collection = db.getCollection("shopliftrUserOffer").withReadPreference(ReadPreference.primaryPreferred());
            MongoCollection<Document> collection1 = db.getCollection("shopliftrOffer").withReadPreference(ReadPreference.primaryPreferred());

            // query parameters
            BasicDBObject query = new BasicDBObject();
            if (flowType == "KNOWN" ||flowType == "TV_KNOWN" ||flowType == "TVKN") {
                query.put("flowType", "TV_KNOWN");
            } else {
                query.put("flowType", "TV_UNKNOWN");
            }
            String DID = filecontent.get(1).split(",")[0];
            query.put("hashes.digitalId", DID);
            query.put("networkId", networkId);
//            if (flowType == "KNOWN" ||flowType == "TV_KNOWN" ||flowType == "TVKN") {
//                query.put("identifier", DID);
//            } else {
//                query.put("identifier", DID.toUpperCase());
//            }

            System.out.println("QUERY: " + query);

            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            JSONArray offers = new JSONArray();
            while (cursor.hasNext()) {
                String data = cursor.next().toJson();
                JSONObject jsonData = new JSONObject(data);
                offers = jsonData.getJSONArray("offers");
            }
            int count = 0;  // matched offers count
            int totalidomoOffers = 5;

            for (int i=1 ; i < totalidomoOffers; i=i+5) {
                String[] fileRow = filecontent.get(i).split(",");
                String brand = fileRow[i+1];/////2,7,12

                String name = fileRow[i+2];
                String additional = fileRow[i+3];
                String image_url = fileRow[i+4];
                String custom_price = fileRow[i+5];
                for (int j = 0; j < offers.length(); j++) {
                    JSONObject offersJson = (JSONObject) offers.get(j);

                    // query parameters
                    BasicDBObject query1 = new BasicDBObject();
                    query1.put("promotion", offersJson.get("promotion"));

                    //mongo db find query
                    FindIterable<Document> fi1 = collection1.find(query1);
                    MongoCursor<Document> cursor1 = fi1.iterator();
                    String data1 = "";
                    while (cursor1.hasNext()) {
                        data1 = cursor1.next().toJson();
                    }
                    JSONObject jsonData = new JSONObject(data1);
                    System.out.println("+++++++++++++++++++Offer DB Data : +++++++++++" + jsonData.get("brand"));
                    if (
                            jsonData.get("brand").equals(brand)&&
                                    (jsonData.get("name").equals(name))
                                    && (jsonData.get("additional").equals(additional))
                                    && (jsonData.get("customPrice").equals(custom_price))
                                    && (jsonData.get("highResImageUrl").equals(image_url))
                    )
                    {
                        result = "MATCHED";
                        count++;
                        break;
                    }
                    result = "NOT MATCHED";
                }
            }
//
            System.out.println("Count : " +count);
//
//                if (count == totalidomoOffers) {
//                    result = "MATCHED";
//                } else {
//                    result = "NOT MATCHED";
//                }


//            mongoClient.close();
//        } catch (Exception e) {
//            System.out.println(e.getCause());
        } finally {
            return result;
        }
    }
}

