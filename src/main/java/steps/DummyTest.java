package steps;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;
import org.bson.Document;
import org.json.JSONObject;
import utils.MongoDBUtils;

public class DummyTest {

    static MongoClient mongoClient=null;
    public static void main(String args[]){

        MongoCollection<Document> collection=null;
        try {
            //String mongoUri="mongodb+srv://mta:y38ngBDNHWmFgLE@sqa-1-tbdu2.azure.mongodb.net/omnimfd?retryWrites=true&readPreference=secondaryPreferred&maxStalenessSeconds=90";
            //,sqa-1-shard-00-01-tbdu2.azure.mongodb.net:27017,sqa-1-shard-00-02-tbdu2.azure.mongodb.net:27017
            String mongoUri="mongodb://mta:y38ngBDNHWmFgLE@sqa-1-shard-00-01-tbdu2.azure.mongodb.net:27017/omnimfd";
            //&authMechanism=SCRAM-SHA-1&replicaSet=atlas-sqa&readPreference=primary&connectTimeouts=6000&socketTimeoutMS=300000?tls=true

            MongoClientURI uri = new MongoClientURI(mongoUri);
             mongoClient = new MongoClient(uri);
            System.out.println("Mongo client connection successful");
            String databaseName = "omnimfd";
            String collectionName = "omniUser";

            MongoDatabase db = mongoClient.getDatabase("omnimfd");
            System.out.println("Database is connected");

            // Fetching the collection from the mongodb
             collection = db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());
             db.getCollection("shopliftrPromotion").withReadPreference(ReadPreference.primaryPreferred());
             BasicDBObject query=new BasicDBObject();
            query.put("networkId","24");
            query.put("identifier","USA-0024-100000187");


            FindIterable<Document> fi = collection.find(query);
            MongoCursor<Document> cursor = fi.iterator();

            while (cursor.hasNext()) {
                String data = cursor.next().toJson();
                System.out.println("Data is"+ data);
//                offers = jsonData.getJSONArray("offers");
//                    System.out.println("Offers Array is : " + offers);
            }


//            mongoClient.close();
            System.out.println("Connection closed");

//            MongoDBUtils mongoDBUtils=new MongoDBUtils();
//            mongoDBUtils.removeOlderOmniUserMongoCollectionDocuments("known","24","24","asdfg1234","100000187");
        }
        catch (Exception e){
            e.printStackTrace();
            mongoClient.close();
            System.out.println("Connection closed");
        }
        finally {
            mongoClient.close();
        }
        }
}
