Feature: Giant Unknown Flow
  #This Feature is to validate below test cases

#  Completed
  @GiantUnknown1
  Scenario Outline: Verify Retailer Landing page when celtra response is success.
    Given remove older omniUser mongo collection documents "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Verify order of elements on retailer landing page
    Then compare offers details on retailer landing page with shopliftrOffer mongoDB collection "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Verify deals are available until upcoming minimum of shopliftrOffer endDate "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>"
    Then Verify Sale prices are valid with your retailer Card only "<retailerName>"
#    Then verify omniUser mongo collection "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"

    Examples:
      | countryCode | networkId |               DID                       |retailerName   |
#      |    USA      |     6.1   | 6F9F0DAD-F9EB-4350-9200-1925B397494D    |Giant          |
#      |    USA      |    4      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | ShopRite       |
#      |    USA      |    6.2    | BF80A2CF-D3AA-49C5-8023-3652BB5F21D9    | MARTIN'S       |
#      |    USA      |     24    | ea39fa2a-4f9c-4c7d-9294-4d8b441d3a4b    | Meijer         |
#      |    USA      |     45    | 2E105C37-E832-40D8-A563-E39C167EDD20    | Weis           |
#      |    USA      |    94     | CE64E669-32F3-494D-A228-9D06D2EA54BC    | Hannaford      |
#      |    USA      |    8      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | Food Lion      |
#      |    USA      |    16     | A8D5F20D-C799-435F-AF5D-223C1130350A    | Hy-Vee         |
#      |    USA      |    6      | 6212350F-6A4D-4D40-B6FF-40B45C2072B7    | Stop & Shop    |


#  Completed
  @GiantUnknown1
  Scenario Outline: Verify Clicking on shop now button
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Verify on clicking shop online button retailer website page is seen "<countryCode>" "<networkId>" "<retailerName>"

    Examples:
      | countryCode | networkId |               DID                       |retailerName   |
#      |    USA      |     6.1   | 6F9F0DAD-F9EB-4350-9200-1925B397494D    |Giant          |
      |    USA      |    4      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | ShopRite       |
      |    USA      |    6.2    | BF80A2CF-D3AA-49C5-8023-3652BB5F21D9    | MARTIN'S       |
#      |    USA      |     24    | ea39fa2a-4f9c-4c7d-9294-4d8b441d3a4b    | Meijer         |
#      |    USA      |     45    | 2E105C37-E832-40D8-A563-E39C167EDD20    | Weis           |
#      |    USA      |    94     | CE64E669-32F3-494D-A228-9D06D2EA54BC    | Hannaford      |
#      |    USA      |    8      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | Food Lion      |
#      |    USA      |    16     | A8D5F20D-C799-435F-AF5D-223C1130350A    | Hy-Vee         |
#      |    USA      |    6      | 6212350F-6A4D-4D40-B6FF-40B45C2072B7    | Stop & Shop    |


#  Completed
  @GiantUnknown1
  Scenario Outline: Validating save to wallet button on landing page.
    Given Extract click thru url from celtra call response "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Verify on clicking save to wallet button wallet pass page is seen "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>" "<retailerName>"

    Examples:
      | countryCode | networkId |               DID                       |retailerName   |
#      |    USA      |     6.1   | 6F9F0DAD-F9EB-4350-9200-1925B397494D    |Giant          |
      |    USA      |    4      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | ShopRite       |
      |    USA      |    6.2    | BF80A2CF-D3AA-49C5-8023-3652BB5F21D9    | MARTIN'S       |
#      |    USA      |     24    | ea39fa2a-4f9c-4c7d-9294-4d8b441d3a4b    | Meijer         |
#      |    USA      |     45    | 2E105C37-E832-40D8-A563-E39C167EDD20    | Weis           |
#      |    USA      |    94     | CE64E669-32F3-494D-A228-9D06D2EA54BC    | Hannaford      |
#      |    USA      |    8      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | Food Lion      |
#      |    USA      |    16     | A8D5F20D-C799-435F-AF5D-223C1130350A    | Hy-Vee         |
#      |    USA      |    6      | 6212350F-6A4D-4D40-B6FF-40B45C2072B7    | Stop & Shop    |

#  Completed
  @GiantUnknown
  Scenario Outline: Verify fallback offer creative for Retailer
    Given Set value of imageUrl field as empty for the first valid promotion "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Given Extract value of fields from celtra call response and verify fallback offer creative info "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"
    Then Navigate to retailer landing page "<countryCode>" "<networkId>" "<DID>" "<retailerName>"
    Then Verify fallback offer creative on retailer landing page for the modified offer "<countryCode>" "<networkId>"
    Then Reset original value of imageUrl field for the first valid promotion "ShopliftrUnknownCeltraUrl" "<countryCode>" "<networkId>" "<DID>"

    Examples:
      | countryCode | networkId |               DID                       |retailerName   |
#      |    USA      |     6.1   | 6F9F0DAD-F9EB-4350-9200-1925B397494D    |Giant          |
      |    USA      |    4      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | ShopRite       |
      |    USA      |    6.2    | BF80A2CF-D3AA-49C5-8023-3652BB5F21D9    | MARTIN'S       |
#      |    USA      |     24    | ea39fa2a-4f9c-4c7d-9294-4d8b441d3a4b    | Meijer         |
#      |    USA      |     45    | 2E105C37-E832-40D8-A563-E39C167EDD20    | Weis           |
#      |    USA      |    94     | CE64E669-32F3-494D-A228-9D06D2EA54BC    | Hannaford      |
#      |    USA      |    8      | 9BB3DF9F-C0EF-43BB-85AF-11270011C4E3    | Food Lion      |
#      |    USA      |    16     | A8D5F20D-C799-435F-AF5D-223C1130350A    | Hy-Vee         |
#      |    USA      |    6      | 6212350F-6A4D-4D40-B6FF-40B45C2072B7    | Stop & Shop    |



#    Other Pending UI test cases:
#  Verify deals expiration date logic for landing page (CircP Shopliftr Known flow)
#  Validate when clicking on offers on landing page, it should NOT navigate to Retailer product search page.
#  Verify "find a Store" Button on Landing page
