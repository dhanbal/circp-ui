package utils;

public class StandardSegmentObject {
	private String segmentId;
	private String[] platform;
	private String reach;
	private boolean analytics;
	private boolean hdfs;
	private boolean postgress;
	private boolean mongoDb;
	
	
	public StandardSegmentObject() {
		super();
	}
		
	public StandardSegmentObject(String segmentId, String[] platform,
			String reach, boolean analytics, boolean hdfs,
			boolean postgress, boolean mongoDb) {
		super();
		this.segmentId = segmentId;
		this.platform = platform;
		this.reach = reach;
		this.analytics = analytics;
		this.hdfs = hdfs;
		this.postgress = postgress;
		this.mongoDb = mongoDb;
	}

	public String getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}

	public String[] getPlatform() {
		return platform;
	}

	public void setPlatform(String[] platform) {
		this.platform = platform;
	}

	public String getReach() {
		return reach;
	}

	public void setReach(String reach) {
		this.reach = reach;
	}

	public boolean isAnalytics() {
		return analytics;
	}

	public void setAnalytics(boolean analytics) {
		this.analytics = analytics;
	}

	public boolean isHdfs() {
		return hdfs;
	}

	public void setHdfs(boolean hdfs) {
		this.hdfs = hdfs;
	}

	public boolean isPostgress() {
		return postgress;
	}

	public void setPostgress(boolean postgress) {
		this.postgress = postgress;
	}

	public boolean isMongoDb() {
		return mongoDb;
	}

	public void setMongoDb(boolean mongoDb) {
		this.mongoDb = mongoDb;
	}

	@Override
	public String toString() {
		String plat="";
		for(int i=0; i<platform.length-1; i++)
			plat=plat+platform[i]+",";
		if(platform.length!=0)
			plat=plat+platform[platform.length-1];
		return "{segmentId:" + segmentId + ", platform:["
				+ plat + "], reach:" + reach + ", analytics:" + analytics
				+ ", hdfs:" + hdfs + ", postgress:" + postgress + ", mongoDb:" + mongoDb+"}";
	}
	
}
