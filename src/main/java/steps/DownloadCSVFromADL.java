package steps;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.ReadPropertyFile;

public class DownloadCSVFromADL {

	 boolean isLinkBroken = false; 	
	 String filepath="C:\\Users\\csauto4\\Documents\\Pavitra Automation\\";
	
	 public String RegistrationSuccessful()
	 {		
		RestAssured.baseURI ="https://restapi.demoqa.com/customer";
		RequestSpecification request = RestAssured.given()
	          .contentType("application/x-www-form-urlencoded")
	            .formParam("grant_type", "client_credentials")
	            .formParam("client_id", "4391188c-35fb-403f-a506-6c1159dcf86d")
	            .formParam("client_secret", "gQx/GjWm.w4.EDpxX84AuBGk=A7M0q]M")
	            .formParam("resource", "https://datalake.azure.net/");	 
		 
		 Response response = request.post("https://login.microsoftonline.com/2eddc39c-2996-4c2a-ab97-f767c39ea155/oauth2/token");
		

		int statusCode = response.getStatusCode();
//		System.out.println("The status code recieved: " + statusCode);
		Assert.assertEquals(statusCode, 200);
		
		JsonPath jsonPathEvaluator = response.jsonPath();		
		String token_type = jsonPathEvaluator.get("token_type");
		String access_token= jsonPathEvaluator.get("access_token");
		String AuthenticationToken= token_type+' '+access_token;
//		System.out.println("Authentication Token: " + AuthenticationToken);
		return AuthenticationToken;
	}
		 
	 public String getCSVFromADL(String filename) throws IOException {
		 String token = RegistrationSuccessful();
		 System.out.println("token " + token);
		 RestAssured.baseURI ="https://dlnpeastus210165137tdcs.azuredatalakestore.net/webhdfs/v1/raw/api/idomoo_csv/"+filename;
			RequestSpecification request1 = RestAssured.given()					
		          .contentType("application/csv")
		          .header("Authorization", token)
		          .queryParam("op" , "OPEN")
		          .queryParam("read" , "true");
		         
			byte[] resp= request1.get().asByteArray();
			 File file=new File(filepath+filename);
	         FileUtils.writeByteArrayToFile(file, resp);
			
			Response response = request1.get();			
			int statusCode = response.getStatusCode();
			Assert.assertEquals(statusCode, 200);					
			String jsonString = response.asString();			
//			System.out.println("Response: " + jsonString);
			return filename;
			}
}






